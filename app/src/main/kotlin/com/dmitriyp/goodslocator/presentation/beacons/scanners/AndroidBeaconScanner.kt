package com.dmitriyp.goodslocator.presentation.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanner
import com.dmitiryp.goodslocator.domain.beacons.scanners.RegionNotifierCallback

internal class AndroidBeaconScanner : BeaconScanner {

    //TODO: to be changed later for real fetching
    fun scan(): List<Beacon> = listOf(beacon, beacon, beacon, beacon, beacon, beacon, beacon, beacon, beacon)

    companion object {

        private val beacon = Beacon()
    }

    override fun startScan(
        beaconScanCallback: BeaconScanCallback,
        beaconRegionNotifierCallback: RegionNotifierCallback
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun stopScan() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}