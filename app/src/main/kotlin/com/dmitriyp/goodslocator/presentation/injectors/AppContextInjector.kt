package com.dmitriyp.goodslocator.presentation.injectors

import android.content.Context

internal object AppContextInjector {

    lateinit var appContext: Context
}