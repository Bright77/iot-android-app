package com.dmitriyp.goodslocator.presentation.viewmodel.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dmitiryp.goodslocator.domain.injectors.ProductDtoProviderInjector.productDtoProvider
import com.dmitriyp.goodslocator.presentation.viewmodel.ProductInfoViewModel
import java.lang.ref.WeakReference

internal class ProductInfoViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        ProductInfoViewModel(productDtoProvider, WeakReference(context)) as T
}