package com.dmitriyp.goodslocator.presentation.persistance

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dmitriyp.goodslocator.presentation.persistance.AppDatabase.Companion.DATABASE_VERSION

@Database(entities = [ProductEntity::class], version = DATABASE_VERSION)
abstract class AppDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    internal companion object {

        const val DATABASE_VERSION = 1
    }
}