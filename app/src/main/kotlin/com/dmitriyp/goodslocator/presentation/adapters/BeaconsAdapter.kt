package com.dmitriyp.goodslocator.presentation.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.adapters.holders.BeaconViewHolder

internal class BeaconsAdapter(
    private val activity: Activity,
    private val productDtoProvider: ProductDtoProvider,
    private val clickListener: AdapterItemClickListener
) :
    ListAdapter<Beacon, BeaconViewHolder>(BeaconDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BeaconViewHolder(
        activity,
        productDtoProvider,
        LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_beacon_preview,
                parent,
                false
            )
    )

    override fun onBindViewHolder(holder: BeaconViewHolder, position: Int) =
        with(holder) {
            bind(getItem(position))
            itemView.setOnClickListener {
                clickListener.onClick(position)
            }
        }

    private companion object BeaconDiffCallback : DiffUtil.ItemCallback<Beacon>() {

        override fun areItemsTheSame(oldItem: Beacon, newItem: Beacon) = oldItem === newItem

        override fun areContentsTheSame(oldItem: Beacon, newItem: Beacon) = oldItem == newItem
    }
}