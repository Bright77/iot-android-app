package com.dmitriyp.goodslocator.presentation.initializers

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.IdRes
import com.dmitiryp.goodslocator.domain.initializers.DistanceInitializer
import com.dmitriyp.goodslocator.R

internal class BeaconViewDistanceInitializer(private val activity: Activity) : DistanceInitializer {

    override fun initializeDistance(distance: Float, @IdRes viewId: Int) =
        activity.runOnUiThread {
            val parentView = activity.findViewById<ViewGroup>(viewId)
            if (distance > 0) {
                parentView.findViewById<TextView>(R.id.distance).text = DISTANCE_TEMPLATE.format(distance)
                parentView.visibility = View.VISIBLE
            } else {
                parentView.visibility = View.INVISIBLE
            }
        }

    private companion object {

        private const val DISTANCE_TEMPLATE = "%.2fm"
    }
}