package com.dmitriyp.goodslocator.presentation.injectors

import androidx.room.Room
import com.dmitriyp.goodslocator.presentation.persistance.AppDatabase

object RoomDatabaseInjector {

    private const val DATABASE_NAME = "goods_locator_db"

    val database by lazy {
        Room.databaseBuilder(
            AppContextInjector.appContext,
            AppDatabase::class.java, DATABASE_NAME
        ).build()
    }
}