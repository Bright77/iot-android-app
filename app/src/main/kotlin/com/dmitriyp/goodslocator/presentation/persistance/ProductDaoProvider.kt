package com.dmitriyp.goodslocator.presentation.persistance

import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.persistance.DaoProvider

internal class ProductDaoProvider(private val productDao: ProductDao) : DaoProvider<ProductDto> {

    override fun getAll(): List<ProductDto> =
        productDao.getAllProducts().map { it.toProductDto() }

    override fun get(id: String) =
        productDao.getProduct(id)?.toProductDto()

    override fun insertAll(items: List<ProductDto>) =
        productDao.insertAll(items.map { it.toProductEntity() })

    override fun deleteAll() =
        productDao.deleteAll()

    private fun ProductDto.toProductEntity() =
        ProductEntity(
            id ?: hashCode().toString(),
            availability,
            dateAdded,
            description,
            imageUrls.first(),
            price,
            title
        )

    private fun ProductEntity.toProductDto() =
        ProductDto(availability, dateAdded, description, listOf(imageUrl), price, title, uuid)
}