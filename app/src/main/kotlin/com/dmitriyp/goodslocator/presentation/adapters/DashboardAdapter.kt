package com.dmitriyp.goodslocator.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dmitiryp.goodslocator.domain.adapters.BindableAdapter
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.adapters.holders.DashboardViewHolder

/**
 * Adapter for dashboard which presents items from bindable [adapter]
 */
class DashboardAdapter(private val adapter: BindableAdapter<ProductDto>) : RecyclerView.Adapter<DashboardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DashboardViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_product_shortcut, parent, false)
        )

    override fun getItemCount() = adapter.getCount()

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        adapter.bind(position) {
            holder.bind(it)
        }
        holder.itemView.setOnClickListener {
            adapter.click(position)
        }
    }

    fun clear() {
        adapter.clear()
        notifyDataSetChanged()
    }

    fun addItems(productList: List<ProductDto>) {
        adapter.addItems(productList)
    }
}