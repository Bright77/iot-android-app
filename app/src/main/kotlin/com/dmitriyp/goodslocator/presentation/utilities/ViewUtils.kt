package com.dmitriyp.goodslocator.presentation.utilities

import android.app.Activity
import android.content.Context
import android.widget.Toast
import androidx.annotation.IdRes
import com.dmitriyp.goodslocator.R
import com.google.android.material.snackbar.Snackbar

fun showToast(context: Context, message: String) =
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

fun showFeatureSnackbarWithAction(
    activity: Activity,
    feature: String, @IdRes viewAnchorId: Int? = null,
    action: () -> Unit
) =
    Snackbar.make(
        activity.findViewById(android.R.id.content),
        activity.getString(R.string.feature_not_enabled_snackbar, feature),
        Snackbar.LENGTH_INDEFINITE
    ).apply {
        setAction(com.dmitriyp.goodslocator.R.string.enable) {
            action()
            dismiss()
        }
        viewAnchorId?.let {
            setAnchorView(it)
        }
        show()
    }