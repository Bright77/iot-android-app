package com.dmitriyp.goodslocator.presentation.permissions

import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.provider.Settings
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager

internal class NfcFeatureManager(private val context: Context) : FeatureManager {

    override val featureName = "NFC"

    override fun isEnabled() = NfcAdapter.getDefaultAdapter(context).isEnabled

    override fun navigateToFeatureScreen() =
        context.startActivity(Intent(Settings.ACTION_NFC_SETTINGS).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        })
}