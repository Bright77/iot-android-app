package com.dmitriyp.goodslocator.presentation.beacons.scanners

import android.content.Intent
import android.content.ServiceConnection
import org.altbeacon.beacon.BeaconConsumer

internal interface NoOpBeaconConsumer : BeaconConsumer {

    override fun getApplicationContext() = null

    override fun unbindService(p0: ServiceConnection?) {}

    override fun bindService(p0: Intent?, p1: ServiceConnection?, p2: Int) = false

    override fun onBeaconServiceConnect() {}

}