package com.dmitriyp.goodslocator.presentation.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconConfiguration
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.beacons.scanners.DomainBeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.DomainRegionNotifierCallback
import com.dmitiryp.goodslocator.domain.injectors.ProductDtoProviderInjector.productDtoProvider
import com.dmitiryp.goodslocator.domain.presenters.BeaconScannerPresenter
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.activities.DirectionActivity
import com.dmitriyp.goodslocator.presentation.adapters.AdapterItemClickListener
import com.dmitriyp.goodslocator.presentation.adapters.BeaconsAdapter
import com.dmitriyp.goodslocator.presentation.beacons.scanners.AltBeaconScanner
import com.dmitriyp.goodslocator.presentation.permissions.BluetoothFeatureManager
import com.dmitriyp.goodslocator.presentation.permissions.LocationFeatureManager
import com.dmitriyp.goodslocator.presentation.permissions.PermissionsInjector.bluetoothPermissionManager
import com.dmitriyp.goodslocator.presentation.permissions.PermissionsInjector.geolocationPermissionManager
import com.dmitriyp.goodslocator.presentation.utilities.showFeatureSnackbarWithAction
import com.dmitriyp.goodslocator.presentation.utilities.showToast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_beacons.*
import kotlinx.android.synthetic.main.include_list_beacons.*
import org.altbeacon.beacon.BeaconManager.getInstanceForApplication
import java.lang.ref.WeakReference

class FindGoodsFragment : Fragment(), BeaconScanView, AdapterItemClickListener {

    private val presenter by lazy {
        val viewRef = WeakReference<BeaconScanView>(this)
        BeaconScannerPresenter(
            viewRef,
            AltBeaconScanner(BeaconConfiguration(), getInstanceForApplication(requireContext())),
            listOf(
                bluetoothPermissionManager(requireActivity()),
                geolocationPermissionManager(requireActivity())
            ),
            listOf(
                LocationFeatureManager(requireContext()),
                BluetoothFeatureManager(requireContext())
            ),
            DomainRegionNotifierCallback(viewRef),
            DomainBeaconScanCallback(viewRef)
        )
    }
    private val adapter by lazy { BeaconsAdapter(requireActivity(), productDtoProvider, this) }
    private var snackbar: WeakReference<Snackbar>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_beacons, container, false)

    override fun onResume() {
        super.onResume()
        presenter.startPresenting()
    }

    override fun showScanningProgress() = requireActivity().runOnUiThread {
        scan_beacons.visibility = VISIBLE
        list_beacons.visibility = GONE
    }

    override fun showFetchedBeacons(fetchedBeacons: List<Beacon>) = requireActivity().runOnUiThread {
        if (list_beacons != null) {
            list_beacons.visibility = VISIBLE
            scan_beacons.visibility = GONE
            beacons_list.adapter = adapter
        }
        adapter.submitList(fetchedBeacons)
        beacon_navigation_button.setOnClickListener {
            onClick(0)
        }
    }

    override fun onPause() {
        super.onPause()
        presenter.stopPresenting()
    }

    override fun onRegionExited(region: BeaconRegion) = requireActivity().runOnUiThread {
        showToast(requireContext(), "Beacon ${region.uuid} region entered")
    }

    override fun onRegionEntered(region: BeaconRegion) = requireActivity().runOnUiThread {
        showToast(requireContext(), "Beacon ${region.uuid} region exited")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            presenter.onRequestPermissionsResult()
        }
    }

    override fun showFeatureNotEnabled(featureName: String) {
        snackbar = WeakReference(
            showFeatureSnackbarWithAction(
                requireActivity(),
                featureName,
                R.id.navigation
            ) {
                presenter.onEnableFeature()
            })
    }

    override fun hideFeatureNotEnabled() {
        snackbar?.get()?.dismiss()
    }

    override fun onClick(position: Int) =
        startActivity(Intent(requireContext(), DirectionActivity::class.java))
}
