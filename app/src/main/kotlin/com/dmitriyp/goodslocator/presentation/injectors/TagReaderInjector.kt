package com.dmitriyp.goodslocator.presentation.injectors

import android.nfc.NdefMessage
import com.dmitiryp.goodslocator.domain.tags.TagReader
import com.dmitriyp.goodslocator.presentation.tags.AndroidTagReader

object TagReaderInjector {

    val tagReader: TagReader<NdefMessage> = AndroidTagReader()
}