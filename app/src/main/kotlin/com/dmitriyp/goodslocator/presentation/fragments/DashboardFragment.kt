package com.dmitriyp.goodslocator.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.dmitiryp.goodslocator.domain.adapters.BindableAdapter
import com.dmitiryp.goodslocator.domain.injectors.DashboardPresenterInjector.dashboardPresenter
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.view.DashboardView
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.adapters.DashboardAdapter
import com.dmitriyp.goodslocator.presentation.activities.ProductInfoActivity.Companion.intent
import com.dmitriyp.goodslocator.presentation.config.Config
import com.dmitriyp.goodslocator.presentation.config.RemoteConfig
import kotlinx.android.synthetic.main.fragment_dashboard.swipe_refresh_layout as swipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_dashboard.products_list as recyclerView

/**
 * Fragment which represents dashboard of the products
 */
class DashboardFragment : Fragment(), DashboardView {

    private val presenter = dashboardPresenter(this)
    private lateinit var adapter: DashboardAdapter
    private val config: Config = RemoteConfig()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_dashboard, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.startPresenting()
        swipeRefreshLayout.setOnRefreshListener {
            presenter.reFetchProducts()
        }
    }

    override fun showAddedItems(startPosition: Int, count: Int) = requireActivity().runOnUiThread {
        adapter.notifyItemRangeInserted(startPosition, count)
    }

    override fun bindAdapter(bindableAdapter: BindableAdapter<ProductDto>) = requireActivity().runOnUiThread {
        adapter = DashboardAdapter(bindableAdapter)
        recyclerView.adapter = adapter

        recyclerView.layoutManager = if(config.isGridLayoutEnabled) {
            GridLayoutManager(requireContext(), 2)
        } else {
            LinearLayoutManager(requireContext())
        }
    }

    override fun addItemsToAdapter(productList: List<ProductDto>) = requireActivity().runOnUiThread {
        adapter.addItems(productList)
    }

    override fun showFetchingError() {
        parentFragment?.findNavController()?.navigate(R.id.action_dashboardFragment_to_noConnectionFragment)
    }

    override fun showFetchingProgress() = requireActivity().runOnUiThread {
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun stopFetchingProgress() = requireActivity().runOnUiThread {
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun clear() = requireActivity().runOnUiThread {
        adapter.clear()
    }

    override fun navigateToProductInfoScreen(productId: String) =
        startActivity(intent(requireContext(), productId))
}
