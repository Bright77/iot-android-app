package com.dmitriyp.goodslocator.presentation.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dmitriyp.goodslocator.R
import kotlinx.android.synthetic.main.activity_payment.pay_by_cache
import kotlinx.android.synthetic.main.activity_payment.pay_by_card

class PaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_payment)

        pay_by_card.setOnClickListener {
            goToResultScreen()
        }

        pay_by_cache.setOnClickListener {
            goToResultScreen()
        }
    }

    private fun goToResultScreen() {
        startActivity(Intent(this, PaymentResultActivity::class.java))
    }
}