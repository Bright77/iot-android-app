package com.dmitriyp.goodslocator.presentation.activities

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.dmitriyp.goodslocator.R
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var connectivityManager: ConnectivityManager
    private val connectivityCallback = object : ConnectivityManager.NetworkCallback() {

        override fun onLost(network: Network?) = runOnUiThread {
            findNavController(R.id.navigationHost).navigate(R.id.action_dashboardFragment_to_noConnectionFragment)
        }

        override fun onAvailable(network: Network?) = runOnUiThread {
            findNavController(R.id.navigationHost).navigate(R.id.action_open_dashboard_fragment)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        setupNavigation()
    }

    private fun setupNavigation() {
        with(findNavController(R.id.navigationHost)) {
            setupActionBarWithNavController(this)
            val bottomNavigationView = findViewById<BottomNavigationView>(R.id.navigation)
            bottomNavigationView.setupWithNavController(this)
        }
    }

    override fun onStart() {
        super.onStart()
        registerNetworkStateCallback()
    }

    private fun registerNetworkStateCallback() =
            with(connectivityManager) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    registerDefaultNetworkCallback(connectivityCallback)
                } else {
                    registerNetworkCallback(NetworkRequest.Builder().build(), connectivityCallback)
                }
            }

    override fun onStop() {
        super.onStop()
        connectivityManager.unregisterNetworkCallback(connectivityCallback)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.navigationHost).navigateUp()
}