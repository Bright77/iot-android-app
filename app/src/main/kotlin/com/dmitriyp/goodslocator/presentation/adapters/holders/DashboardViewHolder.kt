package com.dmitriyp.goodslocator.presentation.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitriyp.goodslocator.presentation.databinding.Bindings.loadPreview
import com.dmitriyp.goodslocator.presentation.databinding.Bindings.setPrice
import kotlinx.android.synthetic.main.item_product_shortcut.view.product_preview_image
import kotlinx.android.synthetic.main.item_product_shortcut.view.product_preview_price
import kotlinx.android.synthetic.main.item_product_shortcut.view.product_preview_title

class DashboardViewHolder(item: View) : RecyclerView.ViewHolder(item) {

    fun bind(product: ProductDto) {
        with(itemView) {
            loadPreview(product_preview_image, product.imageUrls[0])
            setPrice(product_preview_price, product.price)
            product_preview_title.text = product.title
        }
    }
}