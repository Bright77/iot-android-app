package com.dmitriyp.goodslocator.presentation.permissions

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings
import androidx.core.location.LocationManagerCompat
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager

internal class LocationFeatureManager(private val context: Context) : FeatureManager {

    override val featureName = "Location"

    override fun isEnabled() =
        LocationManagerCompat.isLocationEnabled(context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)

    override fun navigateToFeatureScreen() =
        context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        })
}