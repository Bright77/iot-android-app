package com.dmitriyp.goodslocator.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.permissions.NfcFeatureManager
import com.dmitriyp.goodslocator.presentation.utilities.showFeatureSnackbarWithAction
import com.google.android.material.snackbar.Snackbar
import java.lang.ref.WeakReference

class ScanGoodFragment : Fragment() {

    private val nfcFeatureManager by lazy { NfcFeatureManager(requireContext()) }
    private var snackbar: WeakReference<Snackbar>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_scan_good, container, false)

    override fun onResume() {
        super.onResume()
        checkNfc()
    }

    override fun onPause() {
        super.onPause()
        snackbar?.get()?.dismiss()
    }

    private fun checkNfc() {
        if (!nfcFeatureManager.isEnabled()) {
            snackbar = WeakReference(
                showFeatureSnackbarWithAction(
                    requireActivity(),
                    nfcFeatureManager.featureName,
                    R.id.navigation
                ) {
                    nfcFeatureManager.navigateToFeatureScreen()
                })
        }
    }
}
