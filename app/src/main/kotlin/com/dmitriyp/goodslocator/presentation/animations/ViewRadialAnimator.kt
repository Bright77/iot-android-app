package com.dmitriyp.goodslocator.presentation.animations

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.dmitiryp.goodslocator.domain.animations.RadialAnimator
import android.animation.ValueAnimator
import android.view.animation.LinearInterpolator

internal class ViewRadialAnimator(
    private val viewToAnimate: View,
    private val animationDuration: Long = DEFAULT_ANIMATION_DURATION,
    private val degreesOffset: Float = DEFAULT_DEGREES_OFFSET
) : RadialAnimator {

    override fun animate(previousDegree: Float, degrees: Float) {
        ValueAnimator.ofFloat(previousDegree + degreesOffset, degrees + degreesOffset).apply {
            addUpdateListener { valueAnimator ->
                val angle = valueAnimator.animatedValue as Float
                val layoutParams = viewToAnimate.layoutParams as ConstraintLayout.LayoutParams
                layoutParams.circleAngle = angle
                viewToAnimate.layoutParams = layoutParams
            }
            duration = animationDuration
            interpolator = LinearInterpolator()
            repeatMode = ValueAnimator.RESTART
            start()
        }
    }

    private companion object {

        private const val DEFAULT_ANIMATION_DURATION = 1_000L
        private const val DEFAULT_DEGREES_OFFSET = 0f
    }
}