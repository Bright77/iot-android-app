package com.dmitriyp.goodslocator.presentation.config

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig

class RemoteConfig : Config {

    override val isGridLayoutEnabled: Boolean
        get() = Firebase.remoteConfig["grid_layout_enabled"].asBoolean()
}