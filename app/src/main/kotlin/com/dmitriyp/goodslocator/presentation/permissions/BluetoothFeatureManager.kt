package com.dmitriyp.goodslocator.presentation.permissions

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.provider.Settings
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager

internal class BluetoothFeatureManager(private val context: Context) : FeatureManager {

    override val featureName = "Bluetooth"

    override fun isEnabled() = BluetoothAdapter.getDefaultAdapter()?.isEnabled ?: true

    override fun navigateToFeatureScreen() =
        context.startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        })
}