package com.dmitriyp.goodslocator.presentation.utilities

import android.content.Intent

@Suppress("UNCHECKED_CAST")
internal inline fun <reified T> Intent.getTypedParcelableArrayExtra(extra: String): List<T> {
    val dataList = mutableListOf<T>()
    getParcelableArrayExtra(extra).forEach {
        dataList.add(it as T)
    }
    return dataList
}