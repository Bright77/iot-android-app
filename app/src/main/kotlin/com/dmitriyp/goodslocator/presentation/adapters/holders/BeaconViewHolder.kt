package com.dmitriyp.goodslocator.presentation.adapters.holders

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitriyp.goodslocator.presentation.databinding.Bindings
import kotlinx.android.synthetic.main.item_beacon_preview.view.distance_info
import kotlinx.android.synthetic.main.item_beacon_preview.view.product_description
import kotlinx.android.synthetic.main.item_beacon_preview.view.product_logo
import kotlinx.android.synthetic.main.item_beacon_preview.view.product_price
import kotlinx.android.synthetic.main.item_beacon_preview.view.product_title
import kotlinx.android.synthetic.main.item_beacon_preview.view.rssi_info
import kotlinx.android.synthetic.main.item_beacon_preview.view.tx_power_info
import kotlinx.android.synthetic.main.item_beacon_preview.view.uuid_info

internal class BeaconViewHolder(
    private val activity: Activity,
    private val productDtoProvider: ProductDtoProvider,
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(beacon: Beacon) = with(itemView) {
        uuid_info.text = beacon.uuid
        rssi_info.text = beacon.rssi.toString()
        tx_power_info.text = beacon.txPower.toString()
        distance_info.text = DISTANCE_TEMPLATE.format(beacon.distance)

        productDtoProvider.getProductDto(beacon.productId, {
            with(it) {
                activity.runOnUiThread {
                    Bindings.loadPreview(product_logo, imageUrls[0])
                    Bindings.setPrice(product_price, price)
                    product_price.visibility = View.VISIBLE
                    product_title.text = title
                    product_description.text = description
                }
            }
        }, { })

    }

    private companion object {

        private const val DISTANCE_TEMPLATE = "%.2fm"
    }
}