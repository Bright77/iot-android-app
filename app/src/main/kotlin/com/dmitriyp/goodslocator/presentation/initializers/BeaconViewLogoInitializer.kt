package com.dmitriyp.goodslocator.presentation.initializers

import android.app.Activity
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.IdRes
import com.dmitiryp.goodslocator.domain.initializers.LogoInitializer
import com.dmitiryp.goodslocator.domain.injectors.ProductDtoProviderInjector.productDtoProvider
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.activities.ProductInfoActivity
import com.dmitriyp.goodslocator.presentation.databinding.Bindings

internal class BeaconViewLogoInitializer(private val activity: Activity) : LogoInitializer {

    override fun initializeLogo(productId: String, @IdRes viewId: Int) {
        val imageView = activity.findViewById<ViewGroup>(viewId).findViewById<ImageView>(R.id.logo)
        productDtoProvider.getProductDto(productId, { product ->
            activity.runOnUiThread {
                Bindings.loadPreview(imageView, product.imageUrls[0])
            }
        }, {})
        imageView.setOnClickListener {
            activity.startActivity(ProductInfoActivity.intent(activity, productId))
        }
    }
}