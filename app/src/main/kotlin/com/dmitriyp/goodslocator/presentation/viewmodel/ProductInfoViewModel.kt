package com.dmitriyp.goodslocator.presentation.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitriyp.goodslocator.presentation.activities.PaymentActivity
import java.lang.ref.WeakReference

/**
 * View Model for product info screen
 *
 * @param productDtoProvider    the provider of product DTO objects
 * @param contextRef            the context reference
 */
class ProductInfoViewModel(
    private val productDtoProvider: ProductDtoProvider,
    private val contextRef: WeakReference<Context>
) : ViewModel() {

    lateinit var product: ProductDto

    val imageUrl: String by lazy { product.imageUrls[0] }

    val price: Float by lazy { product.price }

    val title: String by lazy { product.title }

    /**
     * Sets [productId] to the viewmodel. Using this Id viewModel fetches needed product from the provider
     */
    fun setProductId(productId: String) {
        productDtoProvider.getProductDto(productId,
            {
                product = it
            }, {}
        )
    }

    /**
     * Opens payment screen for user to buy the product
     */
    fun buyProduct() {
        contextRef.get()?.run {
            startActivity(Intent(this, PaymentActivity::class.java))
        }
    }
}