package com.dmitriyp.goodslocator.presentation.persistance

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.dmitriyp.goodslocator.presentation.persistance.ProductEntity.Companion.TABLE_NAME

@Dao
interface ProductDao {

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAllProducts(): List<ProductEntity>

    @Query("SELECT * FROM $TABLE_NAME WHERE uuid LIKE :id")
    fun getProduct(id: String): ProductEntity?

    @Insert
    fun insertAll(users: List<ProductEntity>)

    @Query("DELETE FROM $TABLE_NAME")
    fun deleteAll()
}