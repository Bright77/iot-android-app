package com.dmitriyp.goodslocator.presentation.config

interface Config {

    val isGridLayoutEnabled: Boolean
}