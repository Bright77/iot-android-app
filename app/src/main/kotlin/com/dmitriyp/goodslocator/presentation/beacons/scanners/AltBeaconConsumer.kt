package com.dmitriyp.goodslocator.presentation.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.RegionNotifierCallback
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoData
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.MonitorNotifier
import org.altbeacon.beacon.Region

private typealias DomainBeacon = com.dmitiryp.goodslocator.domain.beacons.model.Beacon

internal class AltBeaconConsumer(
    private val beaconManager: BeaconManager,
    private val beaconScanCallback: BeaconScanCallback,
    private val beaconRegionNotifierCallback: RegionNotifierCallback
) : NoOpBeaconConsumer {

    override fun onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers()
        beaconManager.removeAllMonitorNotifiers()

        beaconManager.addRangeNotifier { beacons, region ->
            beaconScanCallback.onBeaconsFoundInRegion(beacons.toDomainBeacons(), region.toDomainRegion())
        }

        beaconManager.addMonitorNotifier(object : MonitorNotifier {

            override fun didDetermineStateForRegion(p0: Int, p1: Region?) {}

            override fun didEnterRegion(region: Region) =
                beaconRegionNotifierCallback.onEnteringRegion(region.toDomainRegion())

            override fun didExitRegion(region: Region) =
                beaconRegionNotifierCallback.onExitingRegion(region.toDomainRegion())
        })
    }

    private fun Region.toDomainRegion() = with(this) {
        val identifiers = listOf(id1?.toHexString(), id2?.toHexString(), id3?.toHexString())
        BeaconRegion(identifiers, bluetoothAddress, uniqueId)
    }

    private fun Collection<Beacon>.toDomainBeacons() = with(this) {
        val beacons = mutableListOf<DomainBeacon>()
        forEach {
            with(it) {
                beacons.add(
                    DomainBeacon(
                        id1.toHexString(),
                        distance.toFloat(),
                        rssi,
                        id3.toString(),
                        txPower,
                        GeoData(GeoPositioning.findByDataValue(id2.toInt().toLong()))
                    )
                )
            }
        }
        beacons
    }

    private companion object {

        private const val CATEGORY = "AltBeacon"
    }
}
