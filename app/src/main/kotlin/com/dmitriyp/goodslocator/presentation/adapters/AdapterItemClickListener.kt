package com.dmitriyp.goodslocator.presentation.adapters

internal interface AdapterItemClickListener {

    fun onClick(position: Int)
}