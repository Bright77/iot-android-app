package com.dmitriyp.goodslocator.presentation.permissions

import android.Manifest
import android.app.Activity
import com.dmitiryp.goodslocator.domain.permissions.PermissionManager

internal object PermissionsInjector {

    private const val BLUETOOTH_PERMISSION_RESPONSE = 1
    private const val LOCATION_PERMISSION_RESPONSE = 2

    fun bluetoothPermissionManager(activity: Activity): PermissionManager =
        AndroidPermissionManager(activity, Manifest.permission.BLUETOOTH, BLUETOOTH_PERMISSION_RESPONSE)

    fun geolocationPermissionManager(activity: Activity): PermissionManager =
        AndroidPermissionManager(activity, Manifest.permission.ACCESS_COARSE_LOCATION, LOCATION_PERMISSION_RESPONSE)
}