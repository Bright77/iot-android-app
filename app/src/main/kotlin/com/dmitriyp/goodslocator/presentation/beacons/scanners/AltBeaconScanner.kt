package com.dmitriyp.goodslocator.presentation.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconConfiguration
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanner
import com.dmitiryp.goodslocator.domain.beacons.scanners.RegionNotifierCallback
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.Region

internal class AltBeaconScanner(
    private val beaconConfiguration: BeaconConfiguration,
    private val beaconManager: BeaconManager
) : BeaconScanner {

    private var beaconConsumer: BeaconConsumer? = null

    override fun startScan(
        beaconScanCallback: BeaconScanCallback,
        beaconRegionNotifierCallback: RegionNotifierCallback
    ) = with(beaconManager) {
        setUpBeaconParsers(beaconParsers)
        beaconConsumer = AltBeaconConsumer(beaconManager, beaconScanCallback, beaconRegionNotifierCallback)
        beaconConsumer?.let {
            bind(it)
        }

        startRangingBeaconsInRegion(Region("region", null, null, null))
        startMonitoringBeaconsInRegion(Region("region", null, null, null))
    }

    override fun stopScan() {
        beaconConsumer?.let {
            beaconManager.unbind(it)
        }
        beaconConsumer = null
    }

    private fun setUpBeaconParsers(
        beaconParsers: MutableList<BeaconParser>
    ) = beaconConfiguration.supportedLayouts.forEach {
        beaconParsers.add(BeaconParser().setBeaconLayout(it.value))
    }
}