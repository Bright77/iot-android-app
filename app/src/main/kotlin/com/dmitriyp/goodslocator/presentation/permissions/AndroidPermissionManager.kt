package com.dmitriyp.goodslocator.presentation.permissions

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat.checkSelfPermission
import com.dmitiryp.goodslocator.domain.permissions.PermissionManager

internal class AndroidPermissionManager(
    private val activity: Activity,
    private val neededPermission: String,
    private val permissionResponseCode: Int
) : PermissionManager {

    override fun isAllowed(): Boolean =
        checkSelfPermission(activity, neededPermission) == PackageManager.PERMISSION_GRANTED

    override fun wasPreviouslyDenied() =
        shouldShowRequestPermissionRationale(activity, neededPermission)

    override fun requestPermission() =
        requestPermissions(activity, arrayOf(neededPermission), permissionResponseCode)
}