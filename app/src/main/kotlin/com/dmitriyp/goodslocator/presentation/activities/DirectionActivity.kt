package com.dmitriyp.goodslocator.presentation.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dmitiryp.goodslocator.domain.animations.CompositeRadialAnimator
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconConfiguration
import com.dmitiryp.goodslocator.domain.injectors.AngleProviderInjector.angleProvider
import com.dmitiryp.goodslocator.domain.injectors.ProductDtoProviderInjector.productDtoProvider
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.presenters.DirectionalScanPresenter
import com.dmitiryp.goodslocator.domain.view.DirectionalScanView
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.animations.ViewRadialAnimator
import com.dmitriyp.goodslocator.presentation.beacons.scanners.AltBeaconScanner
import com.dmitriyp.goodslocator.presentation.databinding.Bindings
import com.dmitriyp.goodslocator.presentation.initializers.BeaconViewDistanceInitializer
import com.dmitriyp.goodslocator.presentation.initializers.BeaconViewLogoInitializer
import com.dmitriyp.goodslocator.presentation.permissions.BluetoothFeatureManager
import com.dmitriyp.goodslocator.presentation.permissions.LocationFeatureManager
import com.dmitriyp.goodslocator.presentation.permissions.PermissionsInjector.bluetoothPermissionManager
import com.dmitriyp.goodslocator.presentation.permissions.PermissionsInjector.geolocationPermissionManager
import com.dmitriyp.goodslocator.presentation.utilities.showFeatureSnackbarWithAction
import kotlinx.android.synthetic.main.activity_direction.direction_description
import kotlinx.android.synthetic.main.activity_direction.direction_distance
import kotlinx.android.synthetic.main.activity_direction.product_preview
import kotlinx.android.synthetic.main.activity_direction.reached_product
import kotlinx.android.synthetic.main.include_beacon_compas.blue_beacon
import kotlinx.android.synthetic.main.include_beacon_compas.direction_degrees
import kotlinx.android.synthetic.main.include_beacon_compas.green_beacon
import kotlinx.android.synthetic.main.include_beacon_compas.triangle_arrow
import kotlinx.android.synthetic.main.include_beacon_compas.yellow_beacon
import kotlinx.android.synthetic.main.item_tag_product_preview.product_preview_image
import org.altbeacon.beacon.BeaconManager
import java.lang.ref.WeakReference
import kotlinx.android.synthetic.main.item_tag_product_preview.description as productDescription
import kotlinx.android.synthetic.main.item_tag_product_preview.price as productPrice
import kotlinx.android.synthetic.main.item_tag_product_preview.title as productTitle

class DirectionActivity : AppCompatActivity(), DirectionalScanView {

    private val presenter by lazy {
        DirectionalScanPresenter(
            WeakReference(this),
            AltBeaconScanner(BeaconConfiguration(), BeaconManager.getInstanceForApplication(this)),
            listOf(
                bluetoothPermissionManager(this),
                geolocationPermissionManager(this)
            ),
            listOf(
                LocationFeatureManager(this),
                BluetoothFeatureManager(this)
            ),
            angleProvider(),
            CompositeRadialAnimator(
                listOf(
                    ViewRadialAnimator(green_beacon),
                    ViewRadialAnimator(yellow_beacon, degreesOffset = -15f),
                    ViewRadialAnimator(blue_beacon, degreesOffset = 15f),
                    ViewRadialAnimator(triangle_arrow)
                )
            ),
            productDtoProvider,
            listOf(R.id.green_beacon, R.id.yellow_beacon, R.id.blue_beacon),
            BeaconViewDistanceInitializer(this),
            BeaconViewLogoInitializer(this)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_direction)
    }

    override fun onResume() {
        super.onResume()
        presenter.startPresenting()
    }

    override fun onPause() {
        super.onPause()
        presenter.stopPresenting()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter.onRequestPermissionsResult()
    }

    override fun showDirectionalDegrees(degrees: Float) {
        direction_degrees.text = FORMATTING.format(degrees)
    }

    override fun showFeatureNotEnabled(featureName: String) {
        showFeatureSnackbarWithAction(this, featureName) {
            presenter.onEnableFeature()
        }
    }

    override fun showReachedProduct(productDto: ProductDto) = runOnUiThread {
        reached_product.visibility = View.VISIBLE
        product_preview.visibility = View.VISIBLE
        direction_distance.visibility = View.GONE
        direction_description.visibility = View.GONE

        with(productDto) {
            productTitle.text = title
            productDescription.text = description

            Bindings.setPrice(productPrice, productDto.price)
            Bindings.loadPreview(product_preview_image, productDto.imageUrls[0])

            product_preview.setOnClickListener {
                startActivity(ProductInfoActivity.intent(this@DirectionActivity, this.id!!))
            }
        }
    }

    override fun showClosestDistance(distance: Float) = runOnUiThread {
        reached_product.visibility = View.GONE
        product_preview.visibility = View.GONE
        direction_distance.visibility = View.VISIBLE
        direction_description.visibility = View.VISIBLE
        direction_distance.text = FORMATTING.format(distance)
    }

    private companion object {

        private const val FORMATTING = "%1$,.2f"
    }
}