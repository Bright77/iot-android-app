package com.dmitriyp.goodslocator.presentation.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.databinding.ActivityProductInfoBinding
import com.dmitriyp.goodslocator.presentation.viewmodel.ProductInfoViewModel
import com.dmitriyp.goodslocator.presentation.viewmodel.factory.ProductInfoViewModelFactory

class ProductInfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel =
            ViewModelProvider(this, ProductInfoViewModelFactory(this)).get<ProductInfoViewModel>().apply {
                setProductId(intent?.getStringExtra(PRODUCT_EXTRA)!!)
            }

        DataBindingUtil
            .setContentView<ActivityProductInfoBinding>(this, R.layout.activity_product_info).apply {
                viewmodel = viewModel
            }
    }

    companion object {

        private const val PRODUCT_EXTRA = "product_extra"

        @JvmStatic
        fun intent(context: Context, product: String) =
            Intent(context, ProductInfoActivity::class.java).apply {
                putExtra(PRODUCT_EXTRA, product)
            }
    }
}
