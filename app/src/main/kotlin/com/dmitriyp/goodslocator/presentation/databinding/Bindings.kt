package com.dmitriyp.goodslocator.presentation.databinding

import android.text.Html.FROM_HTML_MODE_LEGACY
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitriyp.goodslocator.R

object Bindings {

    private const val PRICE_PLACEHOLDER = "%1$,.2f PLN"

    /**
     * Binds loading of the image from [previewUrl] into [imageView]
     */
    @JvmStatic
    @BindingAdapter("preview")
    fun loadPreview(imageView: ImageView, imageUrl: String) {
        Glide.with(imageView)
            .load(imageUrl)
            .centerCrop()
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("price")
    fun setPrice(textView: TextView, productPrice: Float) {
        textView.text = formattedPrice(productPrice)
    }

    @JvmStatic
    @BindingAdapter("description")
    fun bindDescription(textView: TextView, product: ProductDto) =
        with(product) {
            textView.text = HtmlCompat.fromHtml(
                textView.context.getString(
                    R.string.product_description,
                    title,
                    availability,
                    dateAdded
                        .replace("Z", " ")
                        .replace("T", " "),
                    formattedPrice(price),
                    description
                ), FROM_HTML_MODE_LEGACY
            )
        }

    private fun formattedPrice(price: Float) = PRICE_PLACEHOLDER.format(price)

}