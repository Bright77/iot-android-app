package com.dmitriyp.goodslocator.presentation

import android.app.Application
import com.dmitiryp.goodslocator.domain.injectors.DaoProviderInjector
import com.dmitriyp.goodslocator.presentation.injectors.AppContextInjector
import com.dmitriyp.goodslocator.presentation.injectors.RoomDatabaseInjector.database
import com.dmitriyp.goodslocator.presentation.persistance.ProductDaoProvider

class PresentationApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        AppContextInjector.appContext = this

        DaoProviderInjector.productDaoProvider = ProductDaoProvider(database.productDao())
    }
}