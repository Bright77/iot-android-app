package com.dmitriyp.goodslocator.presentation.tags

import android.nfc.NdefMessage
import com.dmitiryp.goodslocator.domain.tags.TagReader
import java.nio.charset.Charset

internal class AndroidTagReader : TagReader<NdefMessage> {

    override fun readTag(messagesArray: List<NdefMessage>): String {
        val messagesBuilder = StringBuilder()
        messagesArray.forEach { message ->
            try {
                messagesBuilder.append(String(message.records[0].payload, Charset.forName("UTF-8")))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return messagesBuilder.toString().removeRange(0, TAG_WRITER_PREFIX_RANGE)
    }

    private companion object {

        private const val TAG_WRITER_PREFIX_RANGE = 3
    }
}