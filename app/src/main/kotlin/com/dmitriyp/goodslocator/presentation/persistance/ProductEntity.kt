package com.dmitriyp.goodslocator.presentation.persistance

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dmitriyp.goodslocator.presentation.persistance.ProductEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ProductEntity(

    @PrimaryKey
    val uuid: String,

    @ColumnInfo(name = "availability")
    val availability: Int,

    @ColumnInfo(name = "date_added")
    val dateAdded: String,

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "imageUrl")
    val imageUrl: String,

    @ColumnInfo(name = "price")
    val price: Float,

    @ColumnInfo(name = "title")
    val title: String
) {

    companion object {

        const val TABLE_NAME = "product"
    }
}