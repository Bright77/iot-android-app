package com.dmitriyp.goodslocator.presentation.activities

import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.presenters.TagInfoPresenter
import com.dmitiryp.goodslocator.domain.view.TagInfoView
import com.dmitriyp.goodslocator.R
import com.dmitriyp.goodslocator.presentation.databinding.Bindings.loadPreview
import com.dmitriyp.goodslocator.presentation.databinding.Bindings.setPrice
import com.dmitiryp.goodslocator.domain.injectors.ProductDtoProviderInjector.productDtoProvider
import com.dmitriyp.goodslocator.presentation.injectors.TagReaderInjector.tagReader
import com.dmitriyp.goodslocator.presentation.utilities.getTypedParcelableArrayExtra
import com.dmitriyp.goodslocator.presentation.utilities.showToast
import kotlinx.android.synthetic.main.activity_tag_info.tag_info
import kotlinx.android.synthetic.main.item_tag_product_preview.product_preview_image
import kotlinx.android.synthetic.main.item_tag_product_preview.tag_product_preview
import java.lang.ref.WeakReference
import kotlinx.android.synthetic.main.item_tag_product_preview.description as productDescription
import kotlinx.android.synthetic.main.item_tag_product_preview.price as productPrice
import kotlinx.android.synthetic.main.item_tag_product_preview.title as productTitle

/**
 * Screen with info about tag content and products found using this tag
 */
class TagInfoActivity : AppCompatActivity(), TagInfoView {

    @Suppress("RemoveExplicitTypeArguments")
    private val presenter by lazy {
        TagInfoPresenter<NdefMessage>(
            WeakReference(this),
            productDtoProvider,
            tagReader
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tag_info)

        presenter.startPresenting(intent.getTypedParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES))
    }

    override fun populateViewFromProductDto(productDto: ProductDto) = runOnUiThread {
        with(productDto) {
            productTitle.text = title
            productDescription.text = description

            setPrice(productPrice, productDto.price)
            loadPreview(product_preview_image, productDto.imageUrls[0])

            tag_product_preview.setOnClickListener {
                startActivity(ProductInfoActivity.intent(this@TagInfoActivity, this.id!!))
            }
        }
    }

    override fun showSystemNotHandlingTags() =
        showToast(this, "Tag was not handled correctly")

    override fun updateTagTitle(tag: String) {
        tag_info.text = tag
    }

    override fun showErrorFetchingProductFromTag() =
        showToast(this, "Error fetching productDto from tag")

    override fun checkSystemIsHandlingTags() =
        intent.action == NfcAdapter.ACTION_NDEF_DISCOVERED
}