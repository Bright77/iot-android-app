package com.dmitriyp.goodslocator.data.web.client.typeadapter

import com.dmitriyp.goodslocator.data.model.Product
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

internal class ProductTypeAdapter : TypeAdapter<Product>() {

    override fun write(writer: JsonWriter, value: Product?) {
        throw IllegalStateException()
    }

    override fun read(reader: JsonReader) =
        with(reader) {
            unwrapObject {
                val imageUrlList = mutableListOf<String>()
                var price = 0.0
                var dateAdded = ""
                var title = ""
                var availability = 0
                var description = ""

                while (hasNext()) {
                    when (nextName()) {
                        "images" -> unwrapObject {
                            unwrapFieldObject {
                                unwrapArray {
                                    unwrapPropertyObject {
                                        imageUrlList.add(nextString())
                                    }
                                }
                            }
                        }
                        "price" -> unwrapPropertyObject {
                            price = nextDouble()
                        }
                        "date_added" -> unwrapPropertyObject {
                            dateAdded = nextString()
                        }
                        "title" -> unwrapPropertyObject {
                            title = nextString()
                        }
                        "availability" -> unwrapPropertyObject {
                            availability = nextInt()
                        }
                        "description" -> unwrapPropertyObject {
                            description = nextString()
                        }
                        else -> throw IllegalStateException()
                    }
                }

                Product(availability, dateAdded, description, imageUrlList, price.toFloat(), title)
            }
        }

    private fun <T> JsonReader.unwrapObject(lambda: () -> T): T {
        beginObject()
        val result = lambda()
        endObject()
        return result
    }

    private fun <T> JsonReader.unwrapFieldObject(lambda: () -> T): T {
        nextName()
        return unwrapObject(lambda)
    }

    private fun <T> JsonReader.unwrapPropertyObject(lambda: () -> T): T {
        return unwrapObject {
            nextName()
            lambda()
        }
    }

    private fun JsonReader.unwrapArray(lambda: () -> Unit) {
        nextName()
        beginArray()
        while (hasNext()) {
            lambda()
        }
        endArray()
    }
}