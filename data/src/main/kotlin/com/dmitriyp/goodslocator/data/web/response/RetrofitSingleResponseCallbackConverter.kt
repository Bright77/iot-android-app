package com.dmitriyp.goodslocator.data.web.response

import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse
import retrofit2.Call
import retrofit2.Response

internal class RetrofitSingleResponseCallbackConverter<T>(private val callback: Callback<T>) : retrofit2.Callback<GetResponse<T>> {

    override fun onFailure(call: Call<GetResponse<T>>, t: Throwable) {
        callback.onError()
    }

    override fun onResponse(call: Call<GetResponse<T>>, response: Response<GetResponse<T>>) {
        if(response.isSuccessful) {
            val responseList = mutableListOf<T>()
            response.body()?.let {
                responseList.add(it.appendId())
            }
            callback.onSuccess(responseList)
        } else {
            callback.onError()
        }
    }
}