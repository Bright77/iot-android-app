package com.dmitriyp.goodslocator.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Represents product entity
 */
data class Product(

    @SerializedName("availability")
    val availability: Int,

    @SerializedName("date_added")
    val dateAdded: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("images")
    val imageUrls: List<String>,

    @SerializedName("price")
    val price: Float,

    @SerializedName("title")
    val title: String,

    @Transient
    var id: String? = null

) : Serializable