package com.dmitriyp.goodslocator.data.web.response

import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import com.dmitriyp.goodslocator.data.web.response.model.GetListResponse
import retrofit2.Call
import retrofit2.Response

internal class RetrofitCallbackConverter<T>(private val callback: Callback<T>) : retrofit2.Callback<GetListResponse<T>> {

    override fun onFailure(call: Call<GetListResponse<T>>, t: Throwable) {
        callback.onError()
    }

    override fun onResponse(call: Call<GetListResponse<T>>, response: Response<GetListResponse<T>>) {
        if (response.isSuccessful) {
            val responseList = mutableListOf<T>()
            response.body()?.let { list ->
                list.documents.forEach {
                    responseList.add(it.appendId())
                }
            }
            callback.onSuccess(responseList)
        } else {
            callback.onError()
        }
    }
}