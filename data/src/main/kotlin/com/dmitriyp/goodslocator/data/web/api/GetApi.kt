package com.dmitriyp.goodslocator.data.web.api

import com.dmitriyp.goodslocator.data.model.Category
import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.config.HardcodedWebConfig.CATEGORIES_PATH
import com.dmitriyp.goodslocator.data.web.config.HardcodedWebConfig.PRODUCTS_PATH
import com.dmitriyp.goodslocator.data.web.response.model.GetListResponse
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Fetches List of products
 */
internal interface GetProductsApi {

    /**
     * Returns list of products
     */
    @GET(PRODUCTS_PATH)
    fun get(): Call<GetListResponse<Product>>
}

/**
 * Fetches List of categories
 */
internal interface GetCategoriesApi {

    /**
     * Returns list of categories
     */
    @GET(CATEGORIES_PATH)
    fun get(): Call<GetListResponse<Category>>
}

internal interface GetProductItemApi {

    /**
     * Returns list of products
     */
    @GET("$PRODUCTS_PATH/{item}")
    fun get(@Path("item") productId: String): Call<GetResponse<Product>>
}