package com.dmitriyp.goodslocator.data.model

import com.google.gson.annotations.SerializedName

/**
 * Represents category entity
 */
data class Category(

    @SerializedName("products")
    val productsPathRef: List<String>,

    @SerializedName("sub_categories")
    val subCategoriesPathRef: List<String>,

    @SerializedName("title")
    val title: String
)