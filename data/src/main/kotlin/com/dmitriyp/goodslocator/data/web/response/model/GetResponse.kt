package com.dmitriyp.goodslocator.data.web.response.model

import com.google.gson.annotations.SerializedName

internal data class GetResponse<T>(

    @SerializedName("name")
    val name: String,

    @SerializedName("fields")
    val data: T,

    @SerializedName("createTime")
    val createTime: String,

    @SerializedName("updateTime")
    val updateTime: String
)