package com.dmitriyp.goodslocator.data.web.fetcher.callback

interface Callback<T> {

    fun onSuccess(responseList: List<T>)

    fun onError()
}