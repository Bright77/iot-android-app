package com.dmitriyp.goodslocator.data.web.client

import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.client.typeadapter.ProductTypeAdapter
import com.google.gson.GsonBuilder

/**
 * Gson injector
 */
internal object GsonInjector {

    /**
     * Injects Gson instance
     */
    fun gson() = GsonBuilder()
        .registerTypeAdapter(Product::class.java, ProductTypeAdapter())
        .create()
}