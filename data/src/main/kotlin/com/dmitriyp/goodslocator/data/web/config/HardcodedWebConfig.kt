package com.dmitriyp.goodslocator.data.web.config

/**
 * Hardcoded config for web endpoints
 */
internal object HardcodedWebConfig : WebConfig {

    private const val DELIMITER = "/"
    private const val HTTPS_SCHEME = "https://"
    private const val FIRESTORE_HOST = "firestore.googleapis.com"
    private const val PROJECT_PATH = "v1beta1/projects/goods-locator"
    private const val DATABASE_PATH = "databases/(default)"
    private const val DOCUMENTS_PATH = "documents"

    const val PRODUCTS_PATH = "product"
    const val CATEGORIES_PATH = "category"

    private val baseDocumentsUrl = arrayOf(FIRESTORE_HOST, PROJECT_PATH, DATABASE_PATH, DOCUMENTS_PATH)
        .joinToString(DELIMITER, HTTPS_SCHEME, DELIMITER)


    override val documentsEndpointUrl = baseDocumentsUrl
}