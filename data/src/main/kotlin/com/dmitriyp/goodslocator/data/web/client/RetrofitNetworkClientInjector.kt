package com.dmitriyp.goodslocator.data.web.client

import com.dmitriyp.goodslocator.data.web.client.OkHttpNetworkClientInjector.okHttpNetworkClient
import com.dmitriyp.goodslocator.data.web.config.HardcodedWebConfig
import com.dmitriyp.goodslocator.data.web.config.WebConfig
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Injects retrofit network client
 */
internal object RetrofitNetworkClientInjector {

    /**
     * Returns retrofit network client
     */
    fun retrofitNetworkClient(
        webConfig: WebConfig = HardcodedWebConfig,
        gson: Gson = GsonInjector.gson(),
        okHttpClient: OkHttpClient = okHttpNetworkClient()
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(webConfig.documentsEndpointUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
}