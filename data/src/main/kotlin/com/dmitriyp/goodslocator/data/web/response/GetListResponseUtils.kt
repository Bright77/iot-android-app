package com.dmitriyp.goodslocator.data.web.response

import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse

@Suppress("UNCHECKED_CAST")
internal fun <T> GetResponse<T>.appendId(): T =
    if (data is Product) {
        (data as Product).apply {
            id = name.split("/").last()
        } as T
    } else {
        data
    }