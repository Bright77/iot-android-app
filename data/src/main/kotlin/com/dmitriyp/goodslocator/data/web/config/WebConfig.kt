package com.dmitriyp.goodslocator.data.web.config

/**
 * Config for web endpoints
 */
internal interface WebConfig {

    /**
     * Documents endpoint
     */
    val documentsEndpointUrl: String
}