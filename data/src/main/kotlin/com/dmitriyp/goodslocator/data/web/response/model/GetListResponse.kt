package com.dmitriyp.goodslocator.data.web.response.model

import com.google.gson.annotations.SerializedName

internal data class GetListResponse<T>(

    @SerializedName("documents")
    val documents: List<GetResponse<T>>
)