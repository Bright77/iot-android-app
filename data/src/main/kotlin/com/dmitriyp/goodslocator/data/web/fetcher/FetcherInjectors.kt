package com.dmitriyp.goodslocator.data.web.fetcher

import com.dmitriyp.goodslocator.data.model.Category
import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.api.GetCategoriesApi
import com.dmitriyp.goodslocator.data.web.api.GetProductItemApi
import com.dmitriyp.goodslocator.data.web.api.GetProductsApi
import com.dmitriyp.goodslocator.data.web.client.RetrofitNetworkClientInjector.retrofitNetworkClient
import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import com.dmitriyp.goodslocator.data.web.response.RetrofitCallbackConverter
import com.dmitriyp.goodslocator.data.web.response.RetrofitSingleResponseCallbackConverter

/**
 * Injector of fetchers
 */
object FetcherInjectors {

    /**
     * Fetches products
     */
    fun fetchProducts(callback: Callback<Product>) = retrofitNetworkClient()
        .create(GetProductsApi::class.java)
        .get().enqueue(RetrofitCallbackConverter(callback))

    /**
     * Fetches product item
     */
    fun fetchProductItem(itemId: String, callback: Callback<Product>) = retrofitNetworkClient()
        .create(GetProductItemApi::class.java)
        .get(itemId).enqueue(RetrofitSingleResponseCallbackConverter(callback))

    /**
     * Fetches categories
     */
    fun fetchCategories(callback: Callback<Category>) = retrofitNetworkClient()
        .create(GetCategoriesApi::class.java)
        .get().enqueue(RetrofitCallbackConverter(callback))
}