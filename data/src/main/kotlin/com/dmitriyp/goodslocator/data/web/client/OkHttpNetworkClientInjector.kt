package com.dmitriyp.goodslocator.data.web.client

import okhttp3.OkHttpClient

/**
 * Injects OkHttp network client
 */
internal object OkHttpNetworkClientInjector {

    /**
     * Returns OkHttp network client
     */
    fun okHttpNetworkClient(): OkHttpClient = OkHttpClient.Builder().build()
}