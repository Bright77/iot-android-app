package com.dmitriyp.goodslocator.data.web.response

import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse
import com.google.common.truth.Truth.assertThat
import org.junit.Test

class GetListResponseUtilsTest {

    @Test
    fun `appends correct id from name if Product`() {
        RESPONSE.appendId()

        assertThat(RESPONSE.data.id).isEqualTo(ID)
    }

    private companion object {

        private const val ID = "123"
        private val PRODUCT = Product(0, "", "", emptyList(), 0f, "", null)
        private val RESPONSE = GetResponse("smfth/$ID", PRODUCT, "", "")
    }
}