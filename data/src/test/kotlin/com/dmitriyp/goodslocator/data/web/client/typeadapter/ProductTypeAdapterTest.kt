package com.dmitriyp.goodslocator.data.web.client.typeadapter

import com.dmitriyp.goodslocator.data.model.Product
import com.google.common.truth.Truth.assertThat
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import com.google.gson.stream.MalformedJsonException
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import java.io.CharArrayWriter
import java.io.FileReader
import java.io.StringReader

class ProductTypeAdapterTest {

    private val typeAdapter = ProductTypeAdapter()

    private val gson = GsonBuilder()
        .registerTypeAdapter(Product::class.java, typeAdapter)
        .create()

    @Test
    fun properlyReadsProductJson() {
        val result = gson.fromJson<Product>(
            JsonReader(FileReader("src/test/res/one_product_response.json")),
            Product::class.java
        )
        assertThat(result)
            .isEqualTo(
                Product(
                    AVAILABLE_COUNT,
                    DATE_TIMESTAMP,
                    DESCRIPTION,
                    listOf(IMG_URL_1, IMG_URL_2, IMG_URL_3, IMG_URL_4, IMG_URL_5),
                    PRICE,
                    TITLE
                )
            )
    }

    @Test(expected = IllegalStateException::class)
    fun `throws illegal argument exception on write`() {
        typeAdapter.write(JsonWriter(CharArrayWriter()), null)
    }

    @Test(expected = IllegalStateException::class)
    fun `throws illegal argument exception if next name is unknown`() {
        typeAdapter.read(JsonReader(StringReader("{\"invalid_option\": \"smth\"}")))
    }

    @Test(expected = IllegalStateException::class)
    fun `throws illegal argument exception if nextName() throws illegal argument exception`() {
        val mockReader = mock<JsonReader>()
        whenever(mockReader.hasNext()).thenThrow(IllegalStateException())

        typeAdapter.read(mockReader)
    }

    @Test(expected = MalformedJsonException::class)
    fun `throws malformed argument exception for malformed JSON`() {
        typeAdapter.read(JsonReader(StringReader("{\"}")))
    }

    @Test
    fun `return empty project if json is empty`() {
        val product = typeAdapter.read(JsonReader(StringReader("{}")))
        assertThat(product).isEqualTo(Product(
            0,
            "",
            "",
            emptyList(),
            0f,
            "",
            null
        ))
    }

    private companion object {

        private const val IMG_URL_1 = "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832234.jpg"
        private const val IMG_URL_2 = "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832235.jpg"
        private const val IMG_URL_3 = "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832236.jpg"
        private const val IMG_URL_4 = "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832237.jpg"
        private const val IMG_URL_5 = "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832238.jpg"
        private const val PRICE = 449.9f
        private const val DATE_TIMESTAMP = "2019-04-10T21:53:00Z"
        private const val TITLE = "TOMMY JEANS - PLECAK"
        private const val AVAILABLE_COUNT = 12
        private const val DESCRIPTION =
            "Plecak z kolekcji Tommy Jeans. Model wykonany z gładkiego materiału.  - Jednokomorowy model. - Zapięcie na suwak. - Uchwyt do ręki. - Regulowana długość pasków. - Mieści format A4. - Głębokość: 15 cm. - Wysokość: 41 cm. - Szerokość u podstawy: 30 cm.  Skład: Materiał zasadniczy: 97 % Poliester, 3 % Poliuretan,  ID produktu: 4911-PKM01G  Kod producenta: AU0AU00491"
    }
}