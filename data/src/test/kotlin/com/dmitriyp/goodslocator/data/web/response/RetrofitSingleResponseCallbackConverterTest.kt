package com.dmitriyp.goodslocator.data.web.response

import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.shazam.shazamcrest.matcher.Matchers.sameBeanAs
import okhttp3.ResponseBody
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class RetrofitSingleResponseCallbackConverterTest {

    private val callbackMock = mock<Callback<String>>()
    private val callMock = mock<Call<GetResponse<String>>>()
    private val callbackConverter = RetrofitSingleResponseCallbackConverter(callbackMock)

    @Test
    fun `triggers onError on failure`() {
        callbackConverter.onFailure(callMock, Throwable())

        verify(callbackMock).onError()
    }

    @Test
    fun `triggers onError on unsuccessful response`() {
        callbackConverter.onResponse(callMock, UNSUCCESSFUL_RESPONSE)

        verify(callbackMock).onError()
    }

    @Test
    fun `returns data on successful response`() {
        callbackConverter.onResponse(callMock, SUCCESSFUL_RESPONSE)

        verify(callbackMock).onSuccess(argThat { sameBeanAs(listOf(DATA)).matches(this)})
    }

    @Test
    fun `returns empty list on successful response with null body`() {
        callbackConverter.onResponse(callMock, Response.success(null))

        verify(callbackMock).onSuccess(argThat { sameBeanAs(emptyList<String>()).matches(this)})
    }

    private companion object {

        private const val DATA = "test"
        private val DATA_RESPONSE = GetResponse("", DATA, "", "")
        private val SUCCESSFUL_RESPONSE = Response.success(DATA_RESPONSE)
        private val UNSUCCESSFUL_RESPONSE =
            Response.error<GetResponse<String>>(404, ResponseBody.create(null, ""))
    }
}