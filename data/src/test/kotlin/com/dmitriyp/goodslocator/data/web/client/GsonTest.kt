package com.dmitriyp.goodslocator.data.web.client

import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.client.GsonInjector.gson
import com.dmitriyp.goodslocator.data.web.response.model.GetListResponse
import com.dmitriyp.goodslocator.data.web.response.model.GetResponse
import com.google.common.reflect.TypeToken
import com.google.common.truth.Truth.assertThat
import com.google.gson.stream.JsonReader
import org.junit.Test
import java.io.FileReader

class GsonTest {

    @Test
    fun fullProductsResponseProperlyParsed() {
        val result = gson().fromJson<GetListResponse<Product>>(
            JsonReader(FileReader("src/test/res/full_products_response.json")),
            object : TypeToken<GetListResponse<Product>>(){}.type
        )
        assertThat(result).isEqualTo(EXPECTED)
    }

    private companion object {
        private val EXPECTED = GetListResponse(
            listOf(
                GetResponse(
                    "projects/goods-locator/databases/(default)/documents/product/JjkT5DGl8LBkluTcWkRc",
                    Product(
                        12,
                        "2019-04-10T21:53:00Z",
                        "Plecak z kolekcji Tommy",
                        listOf(
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832234.jpg",
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832235.jpg",
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832236.jpg",
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832237.jpg",
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1832238.jpg"
                        ),
                        449.9f,
                        "TOMMY JEANS - PLECAK"
                    ),
                    "2019-04-10T21:54:35.844116Z",
                    "2019-04-10T21:54:35.844116Z"
                ),
                GetResponse(
                    "projects/goods-locator/databases/(default)/documents/product/OUtF4unL2mwEVjhLH84e",
                    Product(
                        45,
                        "2019-04-10T22:29:18.387536Z",
                        "Bluza z kolekcji Tommy Jeans.",
                        listOf(
                            "https://img.ans-media.com/files/sc_staging_images/product/full_img_1687014.jpg"
                        ),
                        399.0f,
                        "TOMMY JEANS - BLUZA"
                    ),
                    "2019-04-10T22:29:18.387536Z",
                    "2019-04-10T22:29:18.387536Z"
                )
            )
        )
    }
}