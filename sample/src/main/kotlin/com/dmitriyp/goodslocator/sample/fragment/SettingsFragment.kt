package com.dmitriyp.goodslocator.sample.fragment

import android.bluetooth.le.AdvertiseSettings
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreferenceCompat
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconLayout
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.NORTH
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.SOUTH_EAST
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.SOUTH_WEST
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.UNDEFINED
import com.dmitriyp.goodslocator.sample.R
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.BeaconTransmitter

internal class SettingsFragment : PreferenceFragmentCompat() {

    private val beaconParser: BeaconParser
        get() = BeaconParser().setBeaconLayout(BeaconLayout.findByKey(getPreference("beacon_layout")!!)?.value)

    private val beacon: Beacon
        get() = Beacon.Builder()
                .setId1(getPreference("id1"))
                .setId2(getBeaconPositioning())
                .setId3(getPreference("id3"))
                .setManufacturer(getPreference("manufacturer")?.toInt() ?: 0)
                .setTxPower(getPreference("tx_power")?.toInt() ?: -0)
                .build()

    private val beaconTransmitter by lazy { BeaconTransmitter(requireContext(), beaconParser) }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        initDefaultPreferencesValues()
        setPreferencesFromResource(R.xml.beacon_preferences, rootKey)
    }

    private fun initDefaultPreferencesValues() {
        PreferenceManager.getDefaultSharedPreferences(requireActivity())
                .edit()
                .putString("id1", "2f234454-cf6d-4a0f-adf2-f4911ba9ffa6")
                .putString("id2", "1")
                .putString("id3", "12345")
                .putString("manufacturer", "0118")
                .putString("tx_power", "-65")
                .putString("beacon_layout", "ibeacon")
                .apply()
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        if (preference.key == "enable_transmission") {
            if ((preference as SwitchPreferenceCompat).isChecked) {
                startTransmission()
            } else {
                stopTransmission()
            }
        }
        return super.onPreferenceTreeClick(preference)
    }

    private fun getBeaconPositioning() =
                    when (getPreference("beacon_geo_positioning")) {
                        "geo_pos_north" -> NORTH
                        "geo_pos_south_east" -> SOUTH_EAST
                        "geo_pos_south_west" -> SOUTH_WEST
                        else -> UNDEFINED
                    }.dataValue.toString()

    private fun getPreference(key: String) =
            PreferenceManager.getDefaultSharedPreferences(requireActivity())
                    .getString(key, null)

    private fun startTransmission() =
            with(beaconTransmitter) {
                advertiseMode = AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY
                startAdvertising(beacon)
            }

    private fun stopTransmission() =
            beaconTransmitter.stopAdvertising()
}