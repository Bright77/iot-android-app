package com.dmitriyp.goodslocator.sample.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dmitriyp.goodslocator.sample.R
import com.dmitriyp.goodslocator.sample.fragment.SettingsFragment

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.settings,
                SettingsFragment()
            )
            .commit()
    }
}