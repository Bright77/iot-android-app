package com.dmitriyp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.beacons.scanners.DomainRegionNotifierCallback
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import java.lang.ref.WeakReference

class DomainRegionNotifierCallbackTest {

    private val viewMock = mock<BeaconScanView>()
    private val callback = DomainRegionNotifierCallback(WeakReference(viewMock))

    @Test
    fun `shows region entered`() {
        callback.onEnteringRegion(EMPTY_REGION)

        verify(viewMock).onRegionEntered(EMPTY_REGION)
    }

    @Test
    fun `shows region exited`() {
        callback.onExitingRegion(EMPTY_REGION)

        verify(viewMock).onRegionExited(EMPTY_REGION)
    }

    private companion object {

        private val EMPTY_REGION = BeaconRegion(emptyList(), null, null)
    }
}