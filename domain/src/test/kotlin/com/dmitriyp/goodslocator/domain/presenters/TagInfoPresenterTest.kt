package com.dmitriyp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.presenters.TagInfoPresenter
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitiryp.goodslocator.domain.tags.TagReader
import com.dmitiryp.goodslocator.domain.view.TagInfoView
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import java.lang.ref.WeakReference

class TagInfoPresenterTest {

    private val viewMock = mock<TagInfoView>()
    private val productDtoProviderMock = mock<ProductDtoProvider>()
    private val tagReaderMock = mock<TagReader<String>>()

    private val tagInfoPresenter = TagInfoPresenter(WeakReference(viewMock), productDtoProviderMock, tagReaderMock)

    @Test
    fun `checks if system is handling tags`() {
        tagInfoPresenter.startPresenting(tagList)

        verify(viewMock).checkSystemIsHandlingTags()
    }

    @Test
    fun `shows system not handling tags if system not handling tags`() {
        whenever(viewMock.checkSystemIsHandlingTags()).thenReturn(false)

        tagInfoPresenter.startPresenting(tagList)

        verify(viewMock).showSystemNotHandlingTags()
    }

    @Test
    fun `reads tag if system is handling tags`() {
        whenever(viewMock.checkSystemIsHandlingTags()).thenReturn(true)

        tagInfoPresenter.startPresenting(tagList)

        verify(tagReaderMock).readTag(tagList)
    }

    @Test
    fun `updates tag title if system is handling tags`() {
        whenever(viewMock.checkSystemIsHandlingTags()).thenReturn(true)
        whenever(tagReaderMock.readTag(any())).thenReturn(TAG)

        tagInfoPresenter.startPresenting(tagList)

        verify(viewMock).updateTagTitle(TAG)
    }

    @Test
    fun `populates view from product dto`() {
        givenReceivesProductWhileCheckingTheTag()

        verify(viewMock).populateViewFromProductDto(productDto)
    }

    @Test
    fun `shows error fetching product from tags if can not fetch product`() {
        givenDoesNotReceiveProductWhileCheckingTheTag()

        verify(viewMock).showErrorFetchingProductFromTag()
    }

    @Suppress("UNCHECKED_CAST")
    private fun givenReceivesProductWhileCheckingTheTag() {
        whenever(viewMock.checkSystemIsHandlingTags()).thenReturn(true)
        whenever(tagReaderMock.readTag(any())).thenReturn(TAG)
        whenever(productDtoProviderMock.getProductDto(any(), any(), any())).thenAnswer {
            (it.arguments[1] as (product: ProductDto) -> Unit).invoke(productDto)
        }

        tagInfoPresenter.startPresenting(tagList)
    }

    @Suppress("UNCHECKED_CAST")
    private fun givenDoesNotReceiveProductWhileCheckingTheTag() {
        whenever(viewMock.checkSystemIsHandlingTags()).thenReturn(true)
        whenever(tagReaderMock.readTag(any())).thenReturn(TAG)
        whenever(productDtoProviderMock.getProductDto(any(), any(), any())).thenAnswer {
            (it.arguments[2] as () -> Unit).invoke()
        }

        tagInfoPresenter.startPresenting(tagList)
    }

    private companion object {

        private const val TAG = "tag"
        private val tagList = listOf("smth")
        private val productDto = ProductDto(0, "", "", emptyList(), 0f, "")
    }
}