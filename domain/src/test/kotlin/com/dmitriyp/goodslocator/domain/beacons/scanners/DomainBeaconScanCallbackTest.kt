package com.dmitriyp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.beacons.scanners.DomainBeaconScanCallback
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import java.lang.ref.WeakReference

class DomainBeaconScanCallbackTest {

    private val viewMock = mock<BeaconScanView>()
    private val callback = DomainBeaconScanCallback(WeakReference(viewMock))

    @Test
    fun `view shows scanning progress if no beacons were fetched`() {
        callback.onBeaconsFoundInRegion(emptyList(), EMPTY_REGION)

        verify(viewMock).showScanningProgress()
    }

    @Test
    fun `view shows scanned beacons if such are found`() {
        val beacons = listOf(Beacon())
        callback.onBeaconsFoundInRegion(beacons, EMPTY_REGION)

        verify(viewMock).showFetchedBeacons(beacons)
    }

    private companion object {

        private val EMPTY_REGION = BeaconRegion(emptyList(), null, null)
    }
}