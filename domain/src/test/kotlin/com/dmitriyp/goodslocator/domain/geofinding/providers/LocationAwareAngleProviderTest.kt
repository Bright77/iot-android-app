package com.dmitriyp.goodslocator.domain.geofinding.providers

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.geofinding.providers.LocationAwareAngleProvider
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test

@Ignore("Add logic in future")
class LocationAwareAngleProviderTest {

    private val angleProvider = LocationAwareAngleProvider(COMMON_SIDE)

    @Test
    fun `calculates navigation corner between 0 and 90`() {
        assertEquals(EXPECTED_NAVIGATION_CORNER_C, angleProvider.getAngleDegrees(COMMON_LIST))
    }

    @Test
    fun `calculates navigation corner between 90 and 180`() {
        assertEquals(EXPECTED_NAVIGATION_CORNER_C, angleProvider.getAngleDegrees(COMMON_LIST))
    }

    @Test
    fun `calculates navigation corner between 180 and 270`() {
        assertEquals(EXPECTED_NAVIGATION_CORNER_C, angleProvider.getAngleDegrees(COMMON_LIST))
    }

    @Test
    fun `calculates navigation corner between 270 and 360`() {
        assertEquals(EXPECTED_NAVIGATION_CORNER_C, angleProvider.getAngleDegrees(COMMON_LIST))
    }

    private companion object {

        private const val COMMON_SIDE = 5f

        //between 0 and 90
        private const val DISTANCE_C_A = 9.22f
        private const val DISTANCE_C_B = 13.42f
        private const val DISTANCE_C_A_PRIME = 14.03f

        //between 90 and 180
        private const val DISTANCE_H_A = 10.85f
        private const val DISTANCE_H_B = 14.8f
        private const val DISTANCE_H_A_PRIME = 10.69f

        //between 180 and 270
        private const val DISTANCE_G_A = 10.1f
        private const val DISTANCE_G_B = 14.92f
        private const val DISTANCE_G_A_PRIME = 12.11f

        //between 270 and 360
        private const val DISTANCE_I_A = 13.6f
        private const val DISTANCE_I_B = 8.94f
        private const val DISTANCE_I_A_PRIME = 13.4f

        private const val EXPECTED_NAVIGATION_CORNER_C = 54.97821f

        private val BEACON_A_PRIME =
            Beacon(distance = DISTANCE_G_A_PRIME)
        private val BEACON_A = Beacon(distance = DISTANCE_G_A)
        private val BEACON_B = Beacon(distance = DISTANCE_G_B)

        private val COMMON_LIST = listOf(
                BEACON_A_PRIME,
                BEACON_A,
                BEACON_B
        )
    }
}