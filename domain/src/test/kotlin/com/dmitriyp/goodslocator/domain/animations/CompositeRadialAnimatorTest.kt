package com.dmitriyp.goodslocator.domain.animations

import com.dmitiryp.goodslocator.domain.animations.CompositeRadialAnimator
import com.dmitiryp.goodslocator.domain.animations.RadialAnimator
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

class CompositeRadialAnimatorTest {

    private val animatorMock1 = mock<RadialAnimator>()
    private val animatorMock2 = mock<RadialAnimator>()
    private val compositeRadialAnimator = CompositeRadialAnimator(listOf(animatorMock1, animatorMock2))

    @Test
    fun `every animator is called in radial animator`() {
        val degrees = 5f
        val previousDegrees = 3f

        compositeRadialAnimator.animate(previousDegrees, degrees)

        verify(animatorMock1).animate(previousDegrees, degrees)
        verify(animatorMock2).animate(previousDegrees, degrees)
    }
}