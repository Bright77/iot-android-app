package com.dmitriyp.goodslocator.domain.geofinding.providers

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoData
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.*
import com.dmitiryp.goodslocator.domain.geofinding.providers.TriPointsBasedAngleProvider
import org.junit.Assert.assertEquals
import org.junit.Test

class TriPointsBasedAngleProviderTest {

    private val angleProvider = TriPointsBasedAngleProvider()

    @Test
    fun `north east point direction degrees found correctly`() {
        assertEquals(225f, angleProvider.getAngleDegrees(NORTH_EAST_WEST))
    }

    @Test
    fun `north west point direction degrees found correctly`() {
        assertEquals(135f, angleProvider.getAngleDegrees(NORTH_WEST_EAST))
    }

    @Test
    fun `south east point direction degrees found correctly`() {
        assertEquals(315f, angleProvider.getAngleDegrees(EAST_WEST_NORTH))
    }

    @Test
    fun `east point direction degrees found correctly`() {
        assertEquals(270f, angleProvider.getAngleDegrees(EAST_NORTH_WEST))
    }

    @Test
    fun `south west point direction degrees found correctly`() {
        assertEquals(45f, angleProvider.getAngleDegrees(WEST_EAST_NORTH))
    }

    @Test
    fun `west point direction degrees found correctly`() {
        assertEquals(90f, angleProvider.getAngleDegrees(WEST_NORTH_EAST))
    }

    private companion object {

        private const val SMALLEST_DISTANCE = 5f
        private const val MEDIUM_DISTANCE = 6f
        private const val LONGEST_DISTANCE = 7f

        private val NORTH_EAST_WEST =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                )
            )

        private val NORTH_WEST_EAST =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                )
            )

        private val EAST_WEST_NORTH =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                )
            )

        private val EAST_NORTH_WEST =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                )
            )

        private val WEST_EAST_NORTH =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                )
            )

        private val WEST_NORTH_EAST =
            listOf(
                Beacon(
                    distance = SMALLEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_WEST)
                ),
                Beacon(
                    distance = MEDIUM_DISTANCE,
                    geoData = GeoData(positioning = NORTH)
                ),
                Beacon(
                    distance = LONGEST_DISTANCE,
                    geoData = GeoData(positioning = SOUTH_EAST)
                )
            )
    }
}