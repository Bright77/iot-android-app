package com.dmitriyp.goodslocator.domain.jobs

import com.dmitiryp.goodslocator.domain.jobs.ExecutorBasedRepeatableJob
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class ExecutorBasedRepeatableJobTest {

    private val executorServiceMock = mock<ScheduledExecutorService>()
    private val repeatableJob = ExecutorBasedRepeatableJob(INTERVAL, executorServiceMock)

    @Test
    fun `shuts down executor service`() {
        repeatableJob.shutdown()

        verify(executorServiceMock).shutdown()
    }

    @Test
    fun `executes job at fixed rate`() {
        repeatableJob.execute {}

        verify(executorServiceMock).scheduleAtFixedRate(
            any(),
            eq(INITIAL_DELAY),
            eq(INTERVAL),
            eq(TimeUnit.MILLISECONDS)
        )
    }

    private companion object {

        private const val INITIAL_DELAY = 0L
        private const val INTERVAL = 5L
    }
}