package com.dmitriyp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.adapters.BindableAdapter
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.presenters.DashboardPresenter
import com.dmitiryp.goodslocator.domain.providers.ProductListProvider
import com.dmitiryp.goodslocator.domain.view.DashboardView
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import java.lang.ref.WeakReference

class DashboardPresenterTest {

    private val viewMock = mock<DashboardView>()
    private val adapterMock = mock<BindableAdapter<ProductDto>>()
    private val productListProviderMock = mock<ProductListProvider>()

    private val presenter = DashboardPresenter(WeakReference(viewMock), adapterMock, productListProviderMock)

    @Test
    fun `binds adapter on start presenting`() {
        presenter.startPresenting()

        verify(viewMock).bindAdapter(adapterMock)
    }

    @Test
    fun `clears view on start presenting`() {
        presenter.startPresenting()

        verify(viewMock).clear()
    }

    @Test
    fun `shows fetching progress on start presenting`() {
        presenter.startPresenting()

        verify(viewMock).showFetchingProgress()
    }

    @Test
    fun `uses product list provider to get products`() {
        presenter.startPresenting()

        verify(productListProviderMock).getProducts(any(), any())
    }

    @Test
    fun `stops fetching process on received products`() {
        givenReceivesProductsOnStartPresenting()

        verify(viewMock).stopFetchingProgress()
    }

    @Test
    fun `adds items to adapter on received products`() {
        givenReceivesProductsOnStartPresenting()

        verify(viewMock).addItemsToAdapter(products)
    }

    @Test
    fun `binds click listener on received products`() {
        givenReceivesProductsOnStartPresenting()

        verify(adapterMock).bindClickListener(any())
    }

    @Test
    fun `shows added items on received products`() {
        givenReceivesProductsOnStartPresenting()

        verify(viewMock).showAddedItems(0, products.size)
    }

    @Test
    fun `stops fetching progress on fetching error`() {
        givenDoesNotReceiveProductsOnStartPresenting()

        verify(viewMock).stopFetchingProgress()
    }

    @Test
    fun `shows fetching error on fetching error`() {
        givenDoesNotReceiveProductsOnStartPresenting()

        verify(viewMock).showFetchingError()
    }

    @Suppress("UNCHECKED_CAST")
    private fun givenReceivesProductsOnStartPresenting() {
        whenever(productListProviderMock.getProducts(any(), any())).thenAnswer {
            (it.arguments[0] as (products: List<ProductDto>) -> Unit).invoke(products)
        }

        presenter.startPresenting()
    }

    @Suppress("UNCHECKED_CAST")
    private fun givenDoesNotReceiveProductsOnStartPresenting() {
        whenever(productListProviderMock.getProducts(any(), any())).thenAnswer {
            (it.arguments[1] as () -> Unit).invoke()
        }

        presenter.startPresenting()
    }

    private companion object {

        private val products = listOf(ProductDto(0, "", "", emptyList(), 0f, ""))
    }
}