package com.dmitriyp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanner
import com.dmitiryp.goodslocator.domain.beacons.scanners.RegionNotifierCallback
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager
import com.dmitiryp.goodslocator.domain.permissions.PermissionManager
import com.dmitiryp.goodslocator.domain.presenters.BeaconScannerPresenter
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import java.lang.ref.WeakReference

class BeaconScannerPresenterTest {

    private val viewMock = mock<BeaconScanView>()
    private val beaconScannerMock = mock<BeaconScanner>()
    private val permissionManagerMock = mock<PermissionManager>()
    private val featureManagerMock = mock<FeatureManager>()
    private val beaconScanCallbackMock = mock<BeaconScanCallback>()
    private val beaconRegionNotifierCallback = mock<RegionNotifierCallback>()

    private val presenter = BeaconScannerPresenter(
        WeakReference(viewMock),
        beaconScannerMock,
        listOf(permissionManagerMock),
        listOf(featureManagerMock),
        beaconRegionNotifierCallback,
        beaconScanCallbackMock
    )

    @Test
    fun `stops scanning on stopPresenting()`() {
        presenter.stopPresenting()

        verify(beaconScannerMock).stopScan()
    }

    @Test
    fun `hides feature not enabled message on stopPresenting()`() {
        presenter.stopPresenting()

        verify(viewMock).hideFeatureNotEnabled()
    }

    @Test
    fun `requests permission if permissions are not allowed`() {
        whenever(permissionManagerMock.isAllowed()).thenReturn(false)

        presenter.startPresenting()

        verify(permissionManagerMock).requestPermission()
    }

    @Test
    fun `shows feature not enabled if feature is disabled`() {
        val featureName = "smth"
        whenever(permissionManagerMock.isAllowed()).thenReturn(true)
        whenever(featureManagerMock.featureName).thenReturn(featureName)
        whenever(featureManagerMock.isEnabled()).thenReturn(false)

        presenter.startPresenting()

        verify(viewMock).showFeatureNotEnabled(featureName)
    }

    @Test
    fun `navigates to feature screen on onEnableFeature if last disabled feature not null`() {
        whenever(permissionManagerMock.isAllowed()).thenReturn(true)
        whenever(featureManagerMock.isEnabled()).thenReturn(false)

        with(presenter) {
            startPresenting()
            onEnableFeature()
        }

        verify(featureManagerMock).navigateToFeatureScreen()
    }

    @Test
    fun `does not start scan if permission is not allowed`() {
        whenever(permissionManagerMock.isAllowed()).thenReturn(true)

        presenter.startPresenting()

        verifyZeroInteractions(beaconScannerMock)
    }

    @Test
    fun `does not start scan if feature is disabled`() {
        whenever(permissionManagerMock.isAllowed()).thenReturn(true)
        whenever(featureManagerMock.isEnabled()).thenReturn(false)

        presenter.startPresenting()

        verifyZeroInteractions(beaconScannerMock)
    }

    @Test
    fun `starts scan if features and permissions are granted`() {
        whenever(permissionManagerMock.isAllowed()).thenReturn(true)
        whenever(featureManagerMock.isEnabled()).thenReturn(true)

        presenter.startPresenting()

        verify(beaconScannerMock).startScan(beaconScanCallbackMock, beaconRegionNotifierCallback)
    }
}