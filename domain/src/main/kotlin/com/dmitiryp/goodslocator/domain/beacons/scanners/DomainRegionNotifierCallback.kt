package com.dmitiryp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import java.lang.ref.WeakReference

class DomainRegionNotifierCallback(private val viewRef: WeakReference<BeaconScanView>) :
    RegionNotifierCallback {

    private val view: BeaconScanView
        get() = viewRef.get()!!

    override fun onEnteringRegion(region: BeaconRegion) = view.onRegionEntered(region)

    override fun onExitingRegion(region: BeaconRegion) = view.onRegionExited(region)

}