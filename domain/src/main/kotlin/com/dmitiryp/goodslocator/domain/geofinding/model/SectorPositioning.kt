package com.dmitiryp.goodslocator.domain.geofinding.model

enum class SectorPositioning(val degrees: Float) {

    NORTH_EAST(225f),

    NORTH_WEST(135f),

    EAST(270f),

    WEST(90f),

    SOUTH_EAST(315f),

    SOUTH_WEST(45f)
}