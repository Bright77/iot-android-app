package com.dmitiryp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion

class NoOpRegionNotifierCallback: RegionNotifierCallback {

    override fun onEnteringRegion(region: BeaconRegion) {}

    override fun onExitingRegion(region: BeaconRegion) {}
}