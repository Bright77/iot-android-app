package com.dmitiryp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.adapters.BindableAdapter
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.providers.ProductListProvider
import com.dmitiryp.goodslocator.domain.view.DashboardView
import java.lang.ref.WeakReference

/**
 * Presenter of dashboard view
 *
 * @param viewRef               reference to the view
 * @param bindableAdapter       the bindable adapter of list items
 * @param productListProvider   the provider of list of product items
 */
class DashboardPresenter internal constructor(
    private val viewRef: WeakReference<DashboardView>,
    private val bindableAdapter: BindableAdapter<ProductDto>,
    private val productListProvider: ProductListProvider
) {

    private val view
        get() = viewRef.get()!!

    /**
     * Starts presenting
     */
    fun startPresenting() {
        bindAdapter()
        reFetchProducts()
    }

    /**
     * Re fetches products
     */
    fun reFetchProducts() {
        view.clear()
        view.showFetchingProgress()
        productListProvider.getProducts({ productList ->
            with(view) {
                stopFetchingProgress()
                addItemsToAdapter(productList)
                bindableAdapter.bindClickListener {
                    navigateToProductInfoScreen(it.id!!)
                }
                showAddedItems(0, productList.size)
            }
        }, {
           with(view) {
               stopFetchingProgress()
               showFetchingError()
           }
        })
    }

    private fun bindAdapter() {
        view.bindAdapter(bindableAdapter)
    }
}