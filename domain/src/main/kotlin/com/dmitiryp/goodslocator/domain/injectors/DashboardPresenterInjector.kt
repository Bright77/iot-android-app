package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.adapters.BaseAdapter
import com.dmitiryp.goodslocator.domain.injectors.DaoProviderInjector.productDaoProvider
import com.dmitiryp.goodslocator.domain.presenters.DashboardPresenter
import com.dmitiryp.goodslocator.domain.providers.CacheBasedProductListProvider
import com.dmitiryp.goodslocator.domain.view.DashboardView
import java.lang.ref.WeakReference

object DashboardPresenterInjector {

    fun dashboardPresenter(view: DashboardView) = DashboardPresenter(
        WeakReference(view),
        BaseAdapter(),
        CacheBasedProductListProvider(
            productDaoProvider
        )
    )
}