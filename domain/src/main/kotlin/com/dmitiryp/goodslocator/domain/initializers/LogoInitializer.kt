package com.dmitiryp.goodslocator.domain.initializers

interface LogoInitializer {

    fun initializeLogo(productId: String, viewId: Int)
}