package com.dmitiryp.goodslocator.domain.geofinding.providers

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning
import com.dmitiryp.goodslocator.domain.geofinding.model.SectorPositioning
import com.dmitiryp.goodslocator.domain.geofinding.providers.AngleProvider.Companion.HANDLED_BEACONS_SIZE
import com.dmitiryp.goodslocator.domain.geofinding.providers.AngleProvider.Companion.UNHANDLED_DEGREE

internal class TriPointsBasedAngleProvider : AngleProvider {

    override fun getAngleDegrees(beacons: List<Beacon>): Float =
        if (beacons.size == HANDLED_BEACONS_SIZE) {
            val distances = beacons.map { it.distance }

            val minIndex1 = distances.indexOf(distances.min())
            val minIndex2 = distances.indexOf(mutableListOf<Float>().apply {
                addAll(distances)
                remove(distances[minIndex1])
            }.min())

            val minBeacon1 = beacons[minIndex1]
            val minBeacon2 = beacons[minIndex2]

            when (minBeacon1.geoData.positioning) {
                GeoPositioning.NORTH -> {
                    when (minBeacon2.geoData.positioning) {
                        GeoPositioning.SOUTH_EAST -> SectorPositioning.NORTH_EAST.degrees
                        GeoPositioning.SOUTH_WEST -> SectorPositioning.NORTH_WEST.degrees
                        else -> UNHANDLED_DEGREE
                    }
                }
                GeoPositioning.SOUTH_WEST -> {
                    when (minBeacon2.geoData.positioning) {
                        GeoPositioning.SOUTH_EAST -> SectorPositioning.SOUTH_WEST.degrees
                        GeoPositioning.NORTH -> SectorPositioning.WEST.degrees
                        else -> UNHANDLED_DEGREE
                    }
                }
                GeoPositioning.SOUTH_EAST -> {
                    when (minBeacon2.geoData.positioning) {
                        GeoPositioning.SOUTH_WEST -> SectorPositioning.SOUTH_EAST.degrees
                        GeoPositioning.NORTH -> SectorPositioning.EAST.degrees
                        else -> UNHANDLED_DEGREE
                    }
                }
                else -> UNHANDLED_DEGREE
            }
        } else {
            UNHANDLED_DEGREE
        }
}