package com.dmitiryp.goodslocator.domain.geofinding.providers

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoData.Companion.DEFAULT_BEACON_DISTANCE
import com.dmitiryp.goodslocator.domain.geofinding.providers.AngleProvider.Companion.HANDLED_BEACONS_SIZE
import kotlin.math.PI
import kotlin.math.acos
import kotlin.math.pow

internal class LocationAwareAngleProvider(
    private val commonSideLength: Float = DEFAULT_BEACON_DISTANCE
) : AngleProvider {

    override fun getAngleDegrees(beacons: List<Beacon>): Float =
        if (beacons.size == HANDLED_BEACONS_SIZE) {
            (calculateAngle(beacons[0].distance, commonSideLength, beacons[1].distance) +
                    calculateAngle(beacons[1].distance, commonSideLength, beacons[2].distance) +
                    calculateAngle(beacons[0].distance, commonSideLength, beacons[2].distance)) / HANDLED_BEACONS_SIZE
        } else {
            AngleProvider.UNHANDLED_DEGREE
        }

    private fun calculateAngle(commonA: Float, commonB: Float, oppositeC: Float): Float {
        val cos = (commonA.pow(2) + commonB.pow(2) - oppositeC.pow(2)) / (2 * commonA * commonB)
        val acos = acos(cos) * 180f / PI // converting from radians to degrees
        var acosm = acos.toFloat()
        if (acosm > 90f) {
            acosm -= 60f
        } else {
            acosm + 30f
        }
        return acosm
    }
}