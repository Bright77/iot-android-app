package com.dmitiryp.goodslocator.domain.beacons.model

import com.dmitiryp.goodslocator.domain.geofinding.model.GeoData

data class Beacon(

    val uuid: String = DEFAULT_UUID,

    val distance: Float = DEFAULT_DISTANCE,

    val rssi: Int = DEFAULT_RSSI,

    val productId: String = DEFAULT_PRODUCT_ID,

    val txPower: Int = DEFAULT_TX_POWER,

    val geoData: GeoData = DEFAULT_GEO_DATA
) {

    private companion object {

        private const val DEFAULT_UUID = "1234567"
        private const val DEFAULT_DISTANCE= 56f
        private const val DEFAULT_RSSI = -60
        private const val DEFAULT_PRODUCT_ID = "fsadfsfd"
        private const val DEFAULT_TX_POWER = 56
        private val DEFAULT_GEO_DATA = GeoData()
    }
}