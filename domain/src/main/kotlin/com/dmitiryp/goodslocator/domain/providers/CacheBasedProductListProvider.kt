package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.model.ProductDto.Companion.listToDto
import com.dmitiryp.goodslocator.domain.persistance.DaoProvider
import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.fetcher.FetcherInjectors.fetchProducts
import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

internal class CacheBasedProductListProvider(private val productDaoProvider: DaoProvider<ProductDto>) :
    ProductListProvider {

    override fun getProducts(onSuccessBlock: (products: List<ProductDto>) -> Unit, onErrorBlock: () -> Unit) {
        GlobalScope.launch {
            val cachedProducts = productDaoProvider.getAll()
            if (!cachedProducts.isEmpty()) {
                onSuccessBlock(cachedProducts)
            } else {
                fetchProducts(object : Callback<Product> {

                    override fun onSuccess(responseList: List<Product>) {
                        GlobalScope.launch {
                            productDaoProvider.insertAll(listToDto(responseList))
                            onSuccessBlock(listToDto(responseList))
                        }
                    }

                    override fun onError() {
                        onErrorBlock()
                    }
                })
            }
        }
    }
}