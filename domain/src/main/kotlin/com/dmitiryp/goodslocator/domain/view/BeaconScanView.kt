package com.dmitiryp.goodslocator.domain.view

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion

/**
 * Contract for beacon scan view
 */
interface BeaconScanView {

    /**
     * Shows scanning progress
     */
    fun showScanningProgress()

    /**
     * Shows [fetchedBeacons]
     */
    fun showFetchedBeacons(fetchedBeacons: List<Beacon>)

    /**
     * Fires then user exited beacon [region]
     */
    fun onRegionExited(region: BeaconRegion)

    /**
     * Fires then user entered beacon [region]
     */
    fun onRegionEntered(region: BeaconRegion)

    /**
     * Shows feature not enabled with provided [featureName]
     */
    fun showFeatureNotEnabled(featureName: String)

    /**
     * Hides feature not enabled message
     */
    fun hideFeatureNotEnabled()
}