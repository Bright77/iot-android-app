package com.dmitiryp.goodslocator.domain.adapters

/**
 * Represents adapter which can be binded
 */
interface BindableAdapter<T> {

    /**
     * Binds specific view from [position] with adding onClick [action]
     */
    fun bind(position: Int, action: (T) -> Unit)

    /**
     * Returns size of adapter
     */
    fun getCount(): Int

    /**
     * Adds [newItems] to the adapter
     */
    fun addItems(newItems: List<T>)

    /**
     * Clears adapter
     */
    fun clear()

    /**
     * Binds click listener
     */
    fun bindClickListener(newClickListener: (T) -> Unit)

    /**
     * Click of element on [position]
     */
    fun click(position: Int)
}