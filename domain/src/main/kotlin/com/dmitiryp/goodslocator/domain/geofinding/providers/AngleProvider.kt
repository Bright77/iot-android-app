package com.dmitiryp.goodslocator.domain.geofinding.providers

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon

/**
 * Provides angle value
 */
interface AngleProvider {

    /**
     * Returns angle value in degrees for [beacons]
     */
    fun getAngleDegrees(beacons: List<Beacon>): Float

    companion object {

        const val HANDLED_BEACONS_SIZE = 3

        const val UNHANDLED_DEGREE = 1f
    }
}