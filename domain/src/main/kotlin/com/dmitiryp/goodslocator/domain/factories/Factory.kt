package com.dmitiryp.goodslocator.domain.factories

/**
 * Contract for factory
 */
interface Factory<T> {

    /**
     * Returns the created object of type [T]
     */
    fun create(): T
}