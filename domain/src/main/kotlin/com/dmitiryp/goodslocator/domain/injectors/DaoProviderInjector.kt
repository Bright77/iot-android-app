package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.persistance.DaoProvider

object DaoProviderInjector {

    lateinit var productDaoProvider: DaoProvider<ProductDto>
}