package com.dmitiryp.goodslocator.domain.geofinding.model

enum class GeoPositioning(val dataValue: Long) {

    UNDEFINED(-1),

    NORTH(0),

    SOUTH_WEST(1),

    SOUTH_EAST(2);

    companion object {

        fun findByDataValue(dataValue: Long?) = dataValue?.let {
            GeoPositioning.values().find { it.dataValue == dataValue } ?: UNDEFINED
        } ?: UNDEFINED
    }
}