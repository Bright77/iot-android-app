package com.dmitiryp.goodslocator.domain.permissions

interface FeatureManager {

    val featureName: String

    fun isEnabled(): Boolean

    fun navigateToFeatureScreen()

}