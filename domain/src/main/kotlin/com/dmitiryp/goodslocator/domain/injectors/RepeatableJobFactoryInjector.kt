package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.factories.ExecutorBasedRepeatableJobFactory
import com.dmitiryp.goodslocator.domain.factories.Factory
import com.dmitiryp.goodslocator.domain.injectors.BeaconConfigProviderInjector.beaconConfigProvider
import com.dmitiryp.goodslocator.domain.jobs.RepeatableJob

/**
 * Injector for [RepeatableJob]
 */
object RepeatableJobFactoryInjector {

    /**
     * Returns [RepeatableJob] factory
     *
     * @return the repeatable job
     */
    fun repeatableJobFactory(): Factory<RepeatableJob> =
        ExecutorBasedRepeatableJobFactory(beaconConfigProvider().getBeaconConfig().beaconFetchingIntervalMillis)
}