package com.dmitiryp.goodslocator.domain.permissions

interface PermissionManager {

    fun isAllowed(): Boolean

    fun wasPreviouslyDenied(): Boolean

    fun requestPermission()
}