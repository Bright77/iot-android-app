package com.dmitiryp.goodslocator.domain.animations

interface RadialAnimator {

    fun animate(previousDegree: Float, degrees: Float)
}