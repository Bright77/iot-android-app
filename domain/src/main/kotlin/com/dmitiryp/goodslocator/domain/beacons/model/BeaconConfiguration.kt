package com.dmitiryp.goodslocator.domain.beacons.model

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconLayout.*

data class BeaconConfiguration(
    val supportedLayouts: List<BeaconLayout> = listOf(ALTBEACON, IBEACON ,EDDYSTONE_TLM, EDDYSTONE_UID, EDDYSTONE_URL),
    val range: Float = 50.0f
)