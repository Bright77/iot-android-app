package com.dmitiryp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.animations.RadialAnimator
import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanner
import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning
import com.dmitiryp.goodslocator.domain.geofinding.providers.AngleProvider
import com.dmitiryp.goodslocator.domain.initializers.DistanceInitializer
import com.dmitiryp.goodslocator.domain.initializers.LogoInitializer
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager
import com.dmitiryp.goodslocator.domain.permissions.PermissionManager
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitiryp.goodslocator.domain.view.DirectionalScanView
import java.lang.ref.WeakReference

class DirectionalScanPresenter(
    private val viewRef: WeakReference<DirectionalScanView>,
    private val beaconScanner: BeaconScanner,
    private val permissionManagers: List<PermissionManager>,
    private val featureManagers: List<FeatureManager>,
    private val angleProvider: AngleProvider,
    private val radialAnimator: RadialAnimator,
    private val productDtoProvider: ProductDtoProvider,
    private val beaconsViewIds: List<Int>,
    private val distanceInitializer: DistanceInitializer,
    private val logoInitializer: LogoInitializer
) {

    private var lastDisabledFeatureManager: FeatureManager? = null
    private var oldRotationDegree: Float = 0f
    private var initializedLogoViews = mutableListOf<Int>()
    private var isShowingClosestDistance = false

    private val view: DirectionalScanView
        get() = viewRef.get()!!

    fun startPresenting() {
        if (checkPermissions() && checkFeatures()) {
            startScanning()
        }
    }

    private fun startScanning() = beaconScanner.startScan(

        object : BeaconScanCallback {

            override fun onBeaconsFoundInRegion(beacons: List<Beacon>, region: BeaconRegion) {
                val degrees = angleProvider.getAngleDegrees(beacons)
                val closestDistance = beacons.map { it.distance }.min() ?: -1f
                val rotationDegree = closestDistance * 10f

                runBlockAgainstBeacons(beacons) { beacon: Beacon, viewId: Int->
                    distanceInitializer.initializeDistance(beacon.distance, viewId)

                    if(!initializedLogoViews.contains(viewId)) {
                        logoInitializer.initializeLogo(beacon.productId, viewId)
                        initializedLogoViews.add(viewId)
                    }
                }

                radialAnimator.animate(oldRotationDegree, rotationDegree)
                oldRotationDegree = rotationDegree

                view.showDirectionalDegrees(degrees)

                if (MIN_DISTANCE_TRESHOLD > closestDistance && !isShowingClosestDistance) {
                    val id = beacons.find { it.distance == closestDistance }?.productId
                    id?.let {
                        productDtoProvider.getProductDto(id, {
                            view.showReachedProduct(it)
                        }, {
                            view.showClosestDistance(closestDistance)
                        })
                    } ?: view.showClosestDistance(closestDistance)
                    isShowingClosestDistance
                } else {
                    view.showClosestDistance(closestDistance)
                }
            }
        })

    private fun runBlockAgainstBeacons(beacons: List<Beacon>, block: (beacon: Beacon, viewId: Int)-> Unit) {
        //Order of beacons SOUTH_WEST, NORTH, SOUTH_EAST

        for (i in 0..beacons.size) {
            val viewId = beaconsViewIds[i]
            val currentBeacon = findBeaconByPositioning(beacons, GeoPositioning.findByDataValue(i.toLong()))
            currentBeacon?.run {
                block(currentBeacon, viewId)
            }
        }
    }

    private fun findBeaconByPositioning(beacons: List<Beacon>, geoPositioning: GeoPositioning) =
        beacons.find { it.geoData.positioning == geoPositioning }

    private fun checkPermissions(): Boolean {
        permissionManagers.forEach {
            if (!it.isAllowed()) {
                it.requestPermission()
                return false
            }
        }
        return true
    }

    private fun checkFeatures(): Boolean {
        featureManagers.forEach {
            if (!it.isEnabled()) {
                lastDisabledFeatureManager = it
                view.showFeatureNotEnabled(it.featureName)
                return false
            }
        }
        return true
    }

    fun stopPresenting() = beaconScanner.stopScan()

    fun onRequestPermissionsResult() = startPresenting()

    fun onEnableFeature() {
        lastDisabledFeatureManager?.navigateToFeatureScreen()
        lastDisabledFeatureManager = null
    }

    private companion object {

        private const val DEFAULT_DISTANCE = -1f
        private const val MIN_DISTANCE_TRESHOLD = 1.0f

    }
}