package com.dmitiryp.goodslocator.domain.view

import com.dmitiryp.goodslocator.domain.model.ProductDto

/**
 * Contract for tag info view
 */
interface TagInfoView {

    /**
     * Check if system is handling tags
     *
     * @return  true, if system is handling tags
     */
    fun checkSystemIsHandlingTags() : Boolean

    /**
     * Shows system not handling tags dialog
     */
    fun showSystemNotHandlingTags()

    /**
     * Updates title to provided [tag]
     */
    fun updateTagTitle(tag: String)

    /**
     * Shows fetching error dialog
     */
    fun showErrorFetchingProductFromTag()

    /**
     * Populates product information view from [productDto]
     */
    fun populateViewFromProductDto(productDto: ProductDto)
}
