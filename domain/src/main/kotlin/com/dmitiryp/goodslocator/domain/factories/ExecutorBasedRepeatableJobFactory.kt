package com.dmitiryp.goodslocator.domain.factories

import com.dmitiryp.goodslocator.domain.jobs.ExecutorBasedRepeatableJob
import com.dmitiryp.goodslocator.domain.jobs.RepeatableJob
import java.util.concurrent.Executors.newSingleThreadScheduledExecutor

/**
 * Creates the [RepeatableJob] with provided [jobIntervalMillis]
 */
internal class ExecutorBasedRepeatableJobFactory(private val jobIntervalMillis: Long) : Factory<RepeatableJob> {

    override fun create() = ExecutorBasedRepeatableJob(
        jobIntervalMillis,
        newSingleThreadScheduledExecutor()
    )
}