package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.model.ProductDto

internal interface ProductListProvider {

    fun getProducts(onSuccessBlock: (products: List<ProductDto>) -> Unit, onErrorBlock: () -> Unit)
}