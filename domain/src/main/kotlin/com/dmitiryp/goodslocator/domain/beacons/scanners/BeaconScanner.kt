package com.dmitiryp.goodslocator.domain.beacons.scanners

/**
 * Provides list of beacons after scan
 */
interface BeaconScanner {

    fun startScan(beaconScanCallback: BeaconScanCallback = NoOpBeaconScanCallback(),
                  beaconRegionNotifierCallback: RegionNotifierCallback = NoOpRegionNotifierCallback())

    fun stopScan()
}