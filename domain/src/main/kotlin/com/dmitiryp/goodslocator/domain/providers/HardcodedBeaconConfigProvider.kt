package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.config.HardcodedBeaconConfig

class HardcodedBeaconConfigProvider : BeaconConfigProvider {

    override fun getBeaconConfig() = HardcodedBeaconConfig
}