package com.dmitiryp.goodslocator.domain.beacons.model

data class BeaconRegion(
    val identifiers: List<String?>,
    val bluetoothAddress: String?,
    val uuid: String?
)