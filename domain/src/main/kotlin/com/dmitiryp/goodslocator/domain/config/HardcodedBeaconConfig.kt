package com.dmitiryp.goodslocator.domain.config

/**
 * Config of application
 */
object HardcodedBeaconConfig : BeaconConfig {

    /**
     * Beacon fetching interval milliseconds
     */
    override val beaconFetchingIntervalMillis = 2000L
}