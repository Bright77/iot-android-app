package com.dmitiryp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanCallback
import com.dmitiryp.goodslocator.domain.beacons.scanners.BeaconScanner
import com.dmitiryp.goodslocator.domain.beacons.scanners.RegionNotifierCallback
import com.dmitiryp.goodslocator.domain.permissions.FeatureManager
import com.dmitiryp.goodslocator.domain.permissions.PermissionManager
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import java.lang.ref.WeakReference

/**
 * Presenter of [BeaconScanView]
 *
 * @property viewRef                the view reference
 * @property beaconScanner          the beacon scanner
 * @property permissionManagers     the permission managers, which are checking allowed permissions
 * @property featureManagers        the feature managers, which are checking enabled features
 * @property regionNotifierCallback the callback which is returned on entering beacon region
 * @property beaconScanCallback     the callback which is returned on beacon scan completed
 */
class BeaconScannerPresenter(
    private val viewRef: WeakReference<BeaconScanView>,
    private val beaconScanner: BeaconScanner,
    private val permissionManagers: List<PermissionManager>,
    private val featureManagers: List<FeatureManager>,
    private val regionNotifierCallback: RegionNotifierCallback,
    private val beaconScanCallback: BeaconScanCallback
) {

    private var lastDisabledFeatureManager: FeatureManager? = null

    private val view: BeaconScanView
        get() = viewRef.get()!!

    /**
     * Starts presentation of [BeaconScanView]. Firstly all needed permissions and features are checked.
     * If all needed permissions are enabled and features are allowed - beacon scan is starting.
     */
    fun startPresenting() {
        if (checkPermissions() && checkFeatures()) {
            beaconScanner.startScan(beaconScanCallback, regionNotifierCallback)
        }
    }

    private fun checkPermissions(): Boolean {
        permissionManagers.forEach {
            if (!it.isAllowed()) {
                it.requestPermission()
                return false
            }
        }
        return true
    }

    private fun checkFeatures(): Boolean {
        featureManagers.forEach {
            if (!it.isEnabled()) {
                lastDisabledFeatureManager = it
                view.showFeatureNotEnabled(it.featureName)
                return false
            }
        }
        return true
    }

    /**
     * Stops presentation. Requests beacon scanner to stop scanning.
     */
    fun stopPresenting() {
        beaconScanner.stopScan()
        view.hideFeatureNotEnabled()
    }

    /**
     * Invoked that permission result is received.
     */
    fun onRequestPermissionsResult() = startPresenting()

    /**
     * Invoked than feature is enabled
     */
    fun onEnableFeature() {
        lastDisabledFeatureManager?.navigateToFeatureScreen()
        lastDisabledFeatureManager = null
    }
}