package com.dmitiryp.goodslocator.domain.adapters

internal class BaseAdapter<T> : BindableAdapter<T> {

    private val items = mutableListOf<T>()
    private lateinit var clickListener: (T) -> Unit

    override fun bind(position: Int, action: (T) -> Unit) = action(items[position])

    override fun getCount() = items.size

    override fun addItems(newItems: List<T>) {
        items.addAll(newItems)
    }

    override fun clear() {
        items.clear()
    }

    override fun bindClickListener(newClickListener: (T) -> Unit) {
        clickListener = newClickListener
    }

    override fun click(position: Int) = clickListener.invoke(items[position])
}