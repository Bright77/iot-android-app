package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.model.ProductDto

interface ProductDtoProvider {

    fun getProductDto(productId: String, onSuccessBlock: (product: ProductDto) -> Unit, onErrorBlock: () -> Unit)
}