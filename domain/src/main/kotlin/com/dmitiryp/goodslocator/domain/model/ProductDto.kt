package com.dmitiryp.goodslocator.domain.model

import com.dmitriyp.goodslocator.data.model.Product

data class ProductDto(

    val availability: Int,

    val dateAdded: String,

    val description: String,

    val imageUrls: List<String>,

    val price: Float,

    val title: String,

    var id: String? = null
) {

    internal companion object {

        fun listToDto(productList: List<Product>) =
            productList.map { toDto(it) }

        fun toDto(product: Product) =
            with(product) { ProductDto(availability, dateAdded, description, imageUrls, price, title, id) }

    }
}