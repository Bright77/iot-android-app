package com.dmitiryp.goodslocator.domain.animations

class CompositeRadialAnimator(private val radialAnimators: List<RadialAnimator>) : RadialAnimator {

    override fun animate(previousDegree: Float, degrees: Float) =
        radialAnimators.forEach {
            it.animate(previousDegree, degrees)
        }
}