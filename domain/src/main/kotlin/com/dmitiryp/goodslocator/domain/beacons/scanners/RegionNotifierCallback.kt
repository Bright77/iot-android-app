package com.dmitiryp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion

interface RegionNotifierCallback {

    fun onEnteringRegion(region: BeaconRegion)

    fun onExitingRegion(region: BeaconRegion)
}