package com.dmitiryp.goodslocator.domain.presenters

import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider
import com.dmitiryp.goodslocator.domain.tags.TagReader
import com.dmitiryp.goodslocator.domain.view.TagInfoView
import java.lang.ref.WeakReference

class TagInfoPresenter<T>(
    private val viewRef: WeakReference<TagInfoView>,
    private val productDtoProvider: ProductDtoProvider,
    private val tagReader: TagReader<T>
) {

    private val view
        get() = viewRef.get()!!

    fun startPresenting(messagesArray: List<T>) {
        if(view.checkSystemIsHandlingTags()) {
            val tag = tagReader.readTag(messagesArray)
            view.updateTagTitle(tag)
            productDtoProvider.getProductDto(tag, {
                view.populateViewFromProductDto(it)
            }, {
                view.showErrorFetchingProductFromTag()
            })
        } else {
            view.showSystemNotHandlingTags()
        }
    }
}