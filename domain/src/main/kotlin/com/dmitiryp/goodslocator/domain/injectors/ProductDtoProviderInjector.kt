package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.injectors.DaoProviderInjector.productDaoProvider
import com.dmitiryp.goodslocator.domain.providers.CachedBasedProductDtoProvider
import com.dmitiryp.goodslocator.domain.providers.ProductDtoProvider

object ProductDtoProviderInjector {

    val productDtoProvider: ProductDtoProvider = CachedBasedProductDtoProvider(productDaoProvider)
}