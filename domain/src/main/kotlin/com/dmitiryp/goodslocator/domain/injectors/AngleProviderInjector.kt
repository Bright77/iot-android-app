package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.geofinding.providers.AngleProvider
import com.dmitiryp.goodslocator.domain.geofinding.providers.TriPointsBasedAngleProvider

object AngleProviderInjector {

    fun angleProvider(): AngleProvider = TriPointsBasedAngleProvider()
}