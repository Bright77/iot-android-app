package com.dmitiryp.goodslocator.domain.initializers

interface DistanceInitializer {

    fun initializeDistance(distance: Float, viewId: Int)
}