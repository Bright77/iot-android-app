package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.config.BeaconConfig

internal interface BeaconConfigProvider {

    fun getBeaconConfig(): BeaconConfig
}