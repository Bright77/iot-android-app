package com.dmitiryp.goodslocator.domain.injectors

import com.dmitiryp.goodslocator.domain.providers.HardcodedBeaconConfigProvider

internal object BeaconConfigProviderInjector {

    fun beaconConfigProvider() = HardcodedBeaconConfigProvider()
}