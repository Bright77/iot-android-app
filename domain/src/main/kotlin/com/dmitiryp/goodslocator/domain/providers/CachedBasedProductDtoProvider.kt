package com.dmitiryp.goodslocator.domain.providers

import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitiryp.goodslocator.domain.model.ProductDto.Companion.listToDto
import com.dmitiryp.goodslocator.domain.model.ProductDto.Companion.toDto
import com.dmitiryp.goodslocator.domain.persistance.DaoProvider
import com.dmitriyp.goodslocator.data.model.Product
import com.dmitriyp.goodslocator.data.web.fetcher.FetcherInjectors.fetchProductItem
import com.dmitriyp.goodslocator.data.web.fetcher.callback.Callback
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

internal class CachedBasedProductDtoProvider(private val productDaoProvider: DaoProvider<ProductDto>) : ProductDtoProvider {

    override fun getProductDto(productId: String, onSuccessBlock: (product: ProductDto) -> Unit, onErrorBlock: () -> Unit) {
        GlobalScope.launch {
            val cachedProduct = productDaoProvider.get(productId)

            cachedProduct?.let {
                onSuccessBlock(it)
            } ?: fetchProductItem(productId, object : Callback<Product> {

                override fun onSuccess(responseList: List<Product>) {
                    GlobalScope.launch {
                        productDaoProvider.insertAll(listToDto(responseList))
                        onSuccessBlock(toDto(responseList.first()))
                    }
                }

                override fun onError() {
                    onErrorBlock()
                }
            })
        }
    }
}