package com.dmitiryp.goodslocator.domain.view

import com.dmitiryp.goodslocator.domain.model.ProductDto

/**
 * Contract for directional scan view
 */
interface DirectionalScanView {

    /**
     * Shows direction [degrees] to the product
     */
    fun showDirectionalDegrees(degrees: Float)

    /**
     * Shows [feature] not enabled dialog
     */
    fun showFeatureNotEnabled(featureName: String)

    /**
     * Shows [distance] to the closes product
     */
    fun showClosestDistance(distance: Float)

    /**
     * Shows user reached the product. Takes information from [productDto]
     */
    fun showReachedProduct(productDto: ProductDto)
}