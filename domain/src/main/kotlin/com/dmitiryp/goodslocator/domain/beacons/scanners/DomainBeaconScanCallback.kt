package com.dmitiryp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion
import com.dmitiryp.goodslocator.domain.view.BeaconScanView
import java.lang.ref.WeakReference

class DomainBeaconScanCallback(private val viewRef: WeakReference<BeaconScanView>) : BeaconScanCallback {

    private val view: BeaconScanView
        get() = viewRef.get()!!

    override fun onBeaconsFoundInRegion(beacons: List<Beacon>, region: BeaconRegion) {
        if (beacons.isEmpty()) {
            view.showScanningProgress()
        } else {
            view.showFetchedBeacons(beacons)
        }
    }
}