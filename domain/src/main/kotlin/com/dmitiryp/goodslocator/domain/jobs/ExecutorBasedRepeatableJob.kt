package com.dmitiryp.goodslocator.domain.jobs

import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import java.util.concurrent.Executors

/**
 * [RepeatableJob] which is based on [Executors]. Fetches job repeatedly with interval [fetchingJobInterval]
 */
internal class ExecutorBasedRepeatableJob(
    private val fetchingJobInterval: Long,
    private val scheduledExecutorService: ScheduledExecutorService
) : RepeatableJob {

    override fun execute(job: () -> Unit) {
        scheduledExecutorService.scheduleAtFixedRate(
            job,
            0,
            fetchingJobInterval,
            TimeUnit.MILLISECONDS
        )
    }

    override fun shutdown() = scheduledExecutorService.shutdown()
}