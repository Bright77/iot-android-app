package com.dmitiryp.goodslocator.domain.geofinding.model

import com.dmitiryp.goodslocator.domain.geofinding.model.GeoPositioning.UNDEFINED

data class GeoData(

    val positioning: GeoPositioning = UNDEFINED,

    val distanceToNearest: Map<GeoPositioning, Float> = mapOf(
        GeoPositioning.NORTH to DEFAULT_BEACON_DISTANCE,
        GeoPositioning.SOUTH_EAST to DEFAULT_BEACON_DISTANCE,
        GeoPositioning.SOUTH_WEST to DEFAULT_BEACON_DISTANCE)
) {

    companion object {

        const val DEFAULT_BEACON_DISTANCE = 0.5f
    }
}