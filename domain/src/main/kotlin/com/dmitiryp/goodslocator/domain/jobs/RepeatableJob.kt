package com.dmitiryp.goodslocator.domain.jobs

/**
 * Contract for repeatable job
 */
interface RepeatableJob {

    /**
     * Executes repeatedly the specified [job]
     */
    fun execute(job: () -> Unit)

    /**
     * Shuts down the repeatable job
     */
    fun shutdown()
}