package com.dmitiryp.goodslocator.domain.persistance

interface DaoProvider<T> {

    fun getAll(): List<T>

    fun get(id: String): T?

    fun insertAll(items: List<T>)

    fun deleteAll()
}