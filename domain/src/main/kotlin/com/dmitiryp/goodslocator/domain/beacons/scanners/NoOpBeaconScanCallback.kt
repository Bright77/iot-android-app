package com.dmitiryp.goodslocator.domain.beacons.scanners

import com.dmitiryp.goodslocator.domain.beacons.model.Beacon
import com.dmitiryp.goodslocator.domain.beacons.model.BeaconRegion

internal class NoOpBeaconScanCallback : BeaconScanCallback {

    override fun onBeaconsFoundInRegion(beacons: List<Beacon>, region: BeaconRegion) {}
}