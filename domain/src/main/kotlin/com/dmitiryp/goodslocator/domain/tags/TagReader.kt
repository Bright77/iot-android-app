package com.dmitiryp.goodslocator.domain.tags

interface TagReader<T> {

    fun readTag(messagesArray: List<T>): String
}