package com.dmitiryp.goodslocator.domain.config

/**
 * Config of application
 */
interface BeaconConfig {

    /**
     * Beacon fetching interval
     */
    val beaconFetchingIntervalMillis: Long
}