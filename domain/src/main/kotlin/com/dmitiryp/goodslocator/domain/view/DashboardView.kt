package com.dmitiryp.goodslocator.domain.view

import com.dmitiryp.goodslocator.domain.adapters.BindableAdapter
import com.dmitiryp.goodslocator.domain.model.ProductDto
import com.dmitriyp.goodslocator.data.model.Product

/**
 * View contract for the Dashboard
 */
interface DashboardView {

    /**
     * Shows items added from [startPosition] of [count]
     */
    fun showAddedItems(startPosition: Int, count: Int)

    /**
     * Binds [bindableAdapter] to view
     */
    fun bindAdapter(bindableAdapter: BindableAdapter<ProductDto>)

    /**
     * Shows fetching error
     */
    fun showFetchingError()

    /**
     * Shows fetching progress
     */
    fun showFetchingProgress()

    /**
     * Stops fetching progress
     */
    fun stopFetchingProgress()

    /**
     * Clears adapter contents
     */
    fun clear()

    /**
     * Navigates to product info screen
     */
    fun navigateToProductInfoScreen(productId: String)

    /**
     * Adds items to adapter
     */
    fun addItemsToAdapter(productList: List<ProductDto>)
}